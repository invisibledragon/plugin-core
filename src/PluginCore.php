<?php

namespace InvisibleDragon\PluginCore;

/**
 * PluginCore from InvisibleDragon
 *
 * Lightweight classes to do things most plugins need to do
 *
 * @package InvisibleDragon\PluginCore
 */
class PluginCore {

	const PRIVATE_FILES_DIR = 'private_files';

	public static $_cls = null;

	/**
	 * Fully activate the PluginCore functionality inside of WordPress
	 *
	 * This mostly sets up wp_ajax calls
	 */
	public static function activate() {
		add_action( 'wp_ajax_plugincore_update_media', [ static::class, 'update_media_caption' ] );
		add_action( 'wp_ajax_plugincore_get_query_results', [ static::class, 'get_query_results' ] );
		add_action( 'wp_ajax_plugincore_get_media', [ static::class, 'get_media' ] );

		// Private files
		add_filter( 'upload_dir', [ static::class, 'upload_dir' ] );
		add_filter( 'wp_unique_filename', [ static::class, 'update_filename' ], 10, 3 );
		add_filter( 'mod_rewrite_rules', [ static::class, 'private_file_rules' ] );
		add_filter( 'wp_handle_upload', [ static::class, 'handle_private_upload' ] );

		static::$_cls = static::class;

	}

    public static function get_template_part( $template, $args = [], $extra_dir = [] ) {
        if(!is_array($extra_dir)){
            $extra_dir = [$extra_dir];
        }
        foreach( $extra_dir as $dir ) {
            $file = $dir . DIRECTORY_SEPARATOR . $template . '.php';
            if(file_exists( $file )) {
                return require $file;
            }
        }
        $file = static::get_dir() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $template . '.php';
        if(file_exists( $file )) {
            return require $file;
        }
        return get_template_part($template, null, $args);
    }

	public static function get_dir() {
	    throw new \RuntimeException('PluginCore subclass required for this function');
    }

    public static function locate_template($template) {

	    return static::get_dir() . '/templates/' . $template;

    }

	public static function get_media() {
		if(!current_user_can('upload_files')) die('401');

		$media = explode(";", $_GET['media']);
		$query = new \WP_Query([
			'post_type' => 'attachment',
			'post_status' => 'any',
			'post__in' => $media,
			'posts_per_page' => -1,
		]);
		$out = [];
		foreach($query->posts as $post) {
			$out[] = [
				'id' => $post->ID,
				'source_url' => wp_get_attachment_image_src( $post->ID, 'thumbnail' )[0],
				'caption' => $post->post_content,
			];
		}
		wp_send_json($out);

	}

	public static function update_media_caption() {
		if(!current_user_can('upload_files')) die('401');
		if(get_post_type($_GET['media_id']) != 'attachment') die('Invalid');
		$attachment = array(
			'ID' => $_GET['media_id'],
			'post_excerpt' => $_POST['caption']
		);
		// now update main post body
		wp_update_post( $attachment );
	}

    /**
     * @return PluginCore
     */
    public static function get_class() {
        if(!static::$_cls) {
            throw new \RuntimeException('No subclass available');
        }
	    return static::$_cls;
    }

	public static function query_to_json($query) {
        $type = @$query['type'] ?: 'posts';
        switch($type) {
            case 'users':
                $results = get_users($query);
                foreach ($results as $user) {
                    $json[] = [
                        'label' => $user->user_nicename,
                        'value' => (string)$user->ID
                    ];
                }
                break;
            case 'posts':
                $results = new \WP_Query($query);
                foreach ($results->posts as $post) {
                    $json[] = [
                        'label' => $post->post_title,
                        'value' => (string)($post->ID)
                    ];
                }
                break;
        }
		return $json;
	}

	public static function get_query_results() {

		$query = apply_filters('plugincore_query_' . $_GET['auth_path'], null, $_GET['key']);
		if($query) {
			wp_send_json(static::query_to_json(array_merge($query, [
				'per_page' => 10,
				's' => $_GET['term']
			])));
		} else {
			wp_send_json([
				'status' => '_fail'
			], 400);
		}

	}

	public static function get_plugincore_dir() {
		$dir = dirname(plugin_dir_url(__FILE__));
		if(stripos($dir, '/wp-content/themes') !== false) {
			// Turns out we're not a plugin, but a theme!
			$dir = preg_replace( '/^.+vendor/', 'vendor', __FILE__ );
			$dir = dirname(dirname(get_theme_file_uri( $dir )));
		}
		return str_replace('\\', '/', $dir);
	}

	public static function add_admin_css() {
		wp_enqueue_style( 'plugincore-admin-css', static::get_plugincore_dir() . '/assets/admin.css', [], '4' );
	}

	public static function add_admin_js() {
		wp_enqueue_media();
		wp_enqueue_script('plugincore-admin-js', static::get_plugincore_dir() . '/assets/admin.js', ['jquery'] );
	}

	public static function private_file_rules($rewrite) {

		$rule  = "\n# PluginCore Hide Private Files\n\n";
		$rule .= "<IfModule mod_rewrite.c>\n";
		$rule .= "RewriteEngine On\n";
		$rule .= "RewriteRule ^wp-content/uploads/" . static::PRIVATE_FILES_DIR . "/(.*) /404 [R=301,L]\n";
		$rule .= "</IfModule>\n\n";

		return $rule . $rewrite;

	}

	public static function handle_private_upload( $args ) {

		if ( isset( $_POST['type'] ) && 'plugincore_private' === $_POST['type'] ) {
			$args['type'] = 'private/file';
		}
		return $args;

	}

	/**
	 * Change upload dir for downloadable files.
	 *
	 * @param array $pathdata Array of paths.
	 * @return array
	 */
	public static function upload_dir( $pathdata, $use_private = null ) {
		// phpcs:disable WordPress.Security.NonceVerification.Missing
		if($use_private === null && isset( $_POST['type'] )) {
			$use_private = 'plugincore_private' === $_POST['type'];
		}
		if ( $use_private ) {

			if ( empty( $pathdata['subdir'] ) ) {
				$pathdata['path']   = $pathdata['path'] . '/' . static::PRIVATE_FILES_DIR;
				$pathdata['url']    = $pathdata['url'] . '/' . static::PRIVATE_FILES_DIR;
				$pathdata['subdir'] = '/' . static::PRIVATE_FILES_DIR;
			} else {
				$new_subdir = '/' . static::PRIVATE_FILES_DIR . $pathdata['subdir'];

				$pathdata['path']   = str_replace( $pathdata['subdir'], $new_subdir, $pathdata['path'] );
				$pathdata['url']    = str_replace( $pathdata['subdir'], $new_subdir, $pathdata['url'] );
				$pathdata['subdir'] = str_replace( $pathdata['subdir'], $new_subdir, $pathdata['subdir'] );
			}
		}
		return $pathdata;
		// phpcs:enable WordPress.Security.NonceVerification.Missing
	}

	/**
	 * Change filename for WooCommerce uploads and prepend unique chars for security.
	 *
	 * @param string $full_filename Original filename.
	 * @param string $ext           Extension of file.
	 * @param string $dir           Directory path.
	 *
	 * @return string New filename with unique hash.
	 * @since 4.0
	 */
	public static function update_filename( $full_filename, $ext, $dir ) {
		// phpcs:disable WordPress.Security.NonceVerification.Missing
		if ( ! isset( $_POST['type'] ) || ! 'plugincore_private' === $_POST['type'] ) {
			return $full_filename;
		}

		if ( ! strpos( $dir, static::PRIVATE_FILES_DIR ) ) {
			return $full_filename;
		}

		return static::unique_filename( $full_filename, $ext );
		// phpcs:enable WordPress.Security.NonceVerification.Missing
	}

	/**
	 * Change filename to append random text.
	 *
	 * @param string $full_filename Original filename with extension.
	 * @param string $ext           Extension.
	 *
	 * @return string Modified filename.
	 */
	public static function unique_filename( $full_filename, $ext ) {
		$ideal_random_char_length = 6;   // Not going with a larger length because then downloaded filename will not be pretty.
		$max_filename_length      = 255; // Max file name length for most file systems.
		$length_to_prepend        = min( $ideal_random_char_length, $max_filename_length - strlen( $full_filename ) - 1 );

		if ( 1 > $length_to_prepend ) {
			return $full_filename;
		}

		$suffix   = strtolower( wp_generate_password( $length_to_prepend, false, false ) );
		$filename = $full_filename;

		if ( strlen( $ext ) > 0 ) {
			$filename = substr( $filename, 0, strlen( $filename ) - strlen( $ext ) );
		}

		$full_filename = str_replace(
			$filename,
			"$filename-$suffix",
			$full_filename
		);

		return $full_filename;
	}

	public static function value_or_call( $value ) {
		if(is_callable($value)) {
			return call_user_func($value);
		}
		return $value;
	}

	public static function deslash_host( $value ) {
		if(substr($value, strlen($value) - 1) === '/') {
			$value = substr($value, 0, strlen($value) - 1);
		}
		return $value;
	}

}
