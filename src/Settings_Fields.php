<?php

namespace InvisibleDragon\PluginCore;

/**
 * Settings API which allows use of WooCommerce style settings fields without Woo
 *
 * Some of this code is from WooCommerce itself
 *
 * @package InvisibleDragon\PluginCore
 */
class Settings_Fields {

	private $form_fields;
	private $values;
	private $prefix = 'pcmeta__';
    private $suffix = '';
	private $auth_path = '';
    private $instance = '';
    private $tabs = [];

	public function __construct($form_fields, $values = array(), $instance = 'settings', $tabs = []) {
		$this->form_fields = $form_fields;
		$this->values = [];
        $this->instance = $instance;
        $this->tabs = $tabs;
		$this->get_values($values);
	}

	public function get_values( $post_data ) {

        if(!is_array($post_data)) {
            $post_data = [];
        }

        if(array_key_exists( $this->get_settings_field_name(), $post_data) ) {
            $post_data = $post_data[$this->get_settings_field_name()];
            if(is_string($post_data)) {
                $post_data = json_decode( stripslashes($post_data), true );
            }
        }

		foreach ( $this->form_fields as $key => $field ) {
			$this->values[ $key ] = $this->get_field_value($key, $field, $post_data);
		}

		return $this->values;
	}

	/**
	 * Get a field's posted and validated value.
	 *
	 * @param string $key Field key.
	 * @param array  $field Field array.
	 * @param array  $post_data Posted data.
	 * @return string
	 */
	public function get_field_value( $key, $field, $post_data = array() ) {
		$type      = $this->get_field_type( $field );
		$post_data = empty( $post_data ) ? $_POST : $post_data; // WPCS: CSRF ok, input var ok.
		$value     = maybe_unserialize( isset( $post_data[ $key ] ) ? $post_data[ $key ] : null );

        if(is_wp_error($value)) {
            var_dump($key);var_dump($value);
            return null;
        }

		if ( isset( $field['sanitize_callback'] ) && is_callable( $field['sanitize_callback'] ) ) {
			return call_user_func( $field['sanitize_callback'], $value, $field );
		}

		// Look for a validate_FIELDID_field method for special handling.
		if ( is_callable( array( $this, 'validate_' . $key . '_field' ) ) ) {
			return $this->{'validate_' . $key . '_field'}( $key, $value, $field );
		}

		// Look for a validate_FIELDTYPE_field method.
		if ( is_callable( array( $this, 'validate_' . $type . '_field' ) ) ) {
			return $this->{'validate_' . $type . '_field'}( $key, $value, $field );
		}

		// Fallback to text.
		return $this->validate_text_field( $key, $value );
	}

	public function validate_select_field( $key, $value, $field ) {
		$default_option = array_key_first( $field['options'] );
		return $value ?: $field['default'] ?: $default_option;
	}

	/**
	 * Validate Text Field.
	 *
	 * Make sure the data is escaped correctly, etc.
	 *
	 * @param  string $key Field key.
	 * @param  string $value Posted Value.
	 * @return string
	 */
	public function validate_text_field( $key, $value ) {
		$value = is_null( $value ) ? '' : $value;
		return wp_kses_post( trim( stripslashes( $value ) ) );
	}

	public function validate_textarray_field( $key, $value ) {
		return is_array( $value ) ? $value : null;
	}

	public function validate_posts_field( $key, $value ) {
        if(is_string($value)) {
	        return explode(",", $value);
        } elseif(is_array($value)) {
	        return $value;
        }
	}

	public function validate_gallery_field( $key, $value ) {
		if(is_string($value)) {
			return explode(",", $value);
		} elseif(is_array($value)) {
			return $value;
		}
	}

	/**
	 * Sets the authentication path which is used in follow-up AJAX calls
	 * to see if the request is allowed
	 *
	 * @param $auth_path
	 */
	public function set_auth_path($auth_path) {
		$this->auth_path = $auth_path;
	}

	/**
	 * Get a fields type. Defaults to "text" if not set.
	 *
	 * @param  array $field Field key.
	 * @return string
	 */
	public function get_field_type( $field ) {
		return empty( $field['type'] ) ? 'text' : $field['type'];
	}

	/**
	 * Prefix key for settings.
	 *
	 * @param  string $key Field key.
	 * @return string
	 */
	public function get_field_key( $key ) {
		return $this->prefix . $key . $this->suffix;
	}

    public function get_settings_field_name() {
        return 'plugincore-' . PC_Settings_JS::get_prefix() . '-' . $this->instance;
    }

	/**
	 * Generate Settings HTML.
	 *
	 * Generate the HTML for the fields on the "settings" screen.
	 *
	 * @param bool  $echo Echo or return.
	 * @return void|string the html for the settings
	 * @since  1.0.0
	 * @uses   method_exists()
	 */
	public function generate_settings_html( $echo = true ) {

        $fields = $this->form_fields;
        foreach($fields as $key => &$field) {
            if ( !$this->values[$key] ) {
                $this->values[$key] = $field['default'] ?? '';
            }
	        if ( method_exists( $this, 'get_' . $field['type'] . '_settings' ) ) {
                $value = $this->values[ $key ];
		        $field = $this->{'get_' . $field['type'] . '_settings'}( $field, $value );
	        }
        }

		$html = '<div class="plugincore' . PC_Settings_JS::get_prefix() . '-settings-fields" data-auth-path="' . esc_attr($this->auth_path) . '" data-tabs="' . esc_attr(json_encode($this->tabs)) . '" data-fields="' . esc_attr(json_encode($fields)) . '">';
        $html .= '<input type="hidden" name="' . esc_attr($this->get_settings_field_name()) . '" data-orig="' . esc_attr(json_encode($this->values)) . '" value="' . esc_attr(json_encode($this->values)) . '" />';
        $html .= '</div>';

        PC_Settings_JS::enqueue_scripts();

		if ( $echo ) {
			echo $html; // WPCS: XSS ok.
		} else {
			return $html;
		}

	}

	public function get_multiselect_settings($field, $value) {
        return static::get_select_settings($field, $value);
	}

	public function get_select_settings($field, $value) {
		if(is_callable( $field['options'] )) {
			$field['options'] = call_user_func( $field['options'] );
		}
		return $field;
	}

    public function get_image_settings($field, $value) {
        if($value) {
            /*$media = wp_get_attachment_metadata($value);
            $upload_dir = wp_upload_dir();
            $field['media'] = [
                'media_details' => $media,
                'source_url' => $upload_dir['baseurl'] . '/' . $media['file']
            ];*/
        }
        return $field;
    }

    public function get_repeater_settings($field, $value) {
        if(is_array($value)) {
            $field['per_result'] = [];
            foreach($value as $val) {
                $item = [];
                foreach($field['fields'] as $key => $subfield) {
                    if ( method_exists( $this, 'get_' . $subfield['type'] . '_settings' ) ) {
                        $item[$key] = $this->{'get_' . $subfield['type'] . '_settings'}( $subfield, $val[$key] );
                    }
                }
                $field['per_result'][] = $item;
            }
        }
        return $field;
    }

    public function get_post_settings($field, $value) {
        if($value) {
            if(!is_array($value)){
                $value = [$value];
            }
	        $field['options'] = PluginCore::query_to_json(array_merge($field['query'], [
		        'post__in' => $value
	        ]));
        }
        return $field;
    }

    public function get_posts_settings($field, $value) {
        return $this->get_post_settings($field, $value);
    }

    public static function from_tabs( $tabs, $values = array(), $instance = null ) {
        $fields = [];
        $tab_info = [];
        foreach($tabs as $tab) {
            foreach($tab['fields'] as $key => $field) {
                $field['tab'] = $tab['key'];
                $fields[$key] = $field;
            }
            $tab_info[] = [
                'key' => $tab['key'],
                'label' => $tab['label'],
                'icon' => $tab['icon'] ?? null
            ];
        }
        return new Settings_Fields( $fields, $values, $instance, $tab_info );
    }

	public function validate_modal_field( $key, $value, $field ) {
		$item = [];
		foreach($field['fields'] as $key => $subfield) {
			$item[$key] = $this->get_field_value($key, $subfield, $value);
		}
		return $item;
	}

	public function validate_multiselect_field( $key, $value ) {
		if(is_string($value)) {
			return explode(",", $value);
		} elseif(is_array($value)) {
			return $value;
		}
	}

	public function validate_repeater_field( $key, $value, $field ) {
		if(is_array($value)) {
			if(array_key_exists('{TEMPLATE}', $value)) {
				unset($value['{TEMPLATE}']);
			}
			foreach($value as &$item) {
				foreach($field['fields'] as $key => $subfield) {
					$item[$key] = $this->get_field_value($key, $subfield, $item);
				}
			}
		}
		return $value;
	}

	public function validate_datetime_field( $key, $value ) {
		if(is_array($value)) {
			return (new \DateTime( $value['date'], new \DateTimeZone($value['timezone']) ))->format('c');
		}
        return (new \DateTime($value))->format('c');
	}

	public function generate_file_html( $key, $data ) {

		$field_key = $this->get_field_key( $key );
		$value = $this->get_option( $key );

		$attachment = null;
		if($value) {
			$attachment = get_post($value);
			$attachment = $attachment->post_title;
		}

		ob_start();

		?>
        <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
        <fieldset class="plugincore-media" data-type="<?= esc_attr($data['type']); ?>" data-private="<?= esc_attr($data['private'] === true); ?>">
            <div class="plugincore-selected-file"><?= $attachment; ?></div>
            <a href="#" class="button upload_file_button"><?= __('Pick File'); ?></a>
            <input class="plugincore-raw-value" type="hidden" name="<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( $value ); ?>" />
        </fieldset>
		<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
		<?php

		return ob_get_clean();

	}

	public function generate_readonly_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => true,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
        <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
        <input class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>"
               type="text"
               style="<?php echo esc_attr( $data['css'] ); ?>"
               value="<?php echo esc_attr( PluginCore::value_or_call($data['value']) ); ?>"
               placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>"
			<?php disabled( $data['disabled'], true ); ?>
			<?php echo $this->get_custom_attribute_html( $data ); // WPCS: XSS ok. ?>>
		<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
		<?php

		return ob_get_clean();
	}

	/**
	 * Get custom attributes.
	 *
	 * @param  array $data Field data.
	 * @return string
	 */
	public function get_custom_attribute_html( $data ) {
		$custom_attributes = array();

		if ( ! empty( $data['custom_attributes'] ) && is_array( $data['custom_attributes'] ) ) {
			foreach ( $data['custom_attributes'] as $attribute => $attribute_value ) {
				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
			}
		}

		return implode( ' ', $custom_attributes );
	}

	public function get_option($key) {
		return $this->values[$key];
	}

	/**
	 * Get HTML for descriptions.
	 *
	 * @param  array $data Data for the description.
	 * @return string
	 */
	public function get_description_html( $data ) {
		if ( true === $data['desc_tip'] ) {
			$description = '';
		} elseif ( ! empty( $data['desc_tip'] ) ) {
			$description = $data['description'];
		} elseif ( ! empty( $data['description'] ) ) {
			$description = $data['description'];
		} else {
			$description = '';
		}

		return $description ? '<p class="description">' . wp_kses_post( $description ) . '</p>' . "\n" : '';
	}

    public static function find_field($fields, $key) {
        if($fields[$key]) return $fields[$key];
        $parts = explode(".", $key);
        $end = array_pop($parts);
        foreach($parts as $part) {
            if($fields[$part]) {
                $fields = $fields[$part];
                if(array_key_exists('fields', $fields)) {
                    $fields = $fields['fields'];
                }
            } else {
                return null;
            }
        }
        return $fields[$end];
    }

}
