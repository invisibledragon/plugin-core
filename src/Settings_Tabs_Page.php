<?php

namespace InvisibleDragon\PluginCore;

/**
 * A settings page with tabs
 */
abstract class Settings_Tabs_Page extends Settings_Page {

	public static function get_fields() {
		$fields = [];
		$tabs = static::get_tabs();
		foreach($tabs as $tab) {
			$fields = array_merge($fields, $tab['fields']);
		}
		return $fields;
	}

	public static function get_settings() {
		$options = static::get_options();
		return Settings_Fields::from_tabs(static::get_tabs(), $options);
	}

	public abstract static function get_tabs();

}
