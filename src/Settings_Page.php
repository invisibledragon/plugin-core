<?php

namespace InvisibleDragon\PluginCore;

abstract class Settings_Page {

	public abstract static function get_fields();

	public abstract static function get_title();

	public abstract static function get_menu_slug();

	public abstract static function get_setting_key();

	public static function activate() {
		add_action( 'admin_menu', [ static::class, 'init' ] );
		add_filter( 'plugincore_query_settings_' . static::get_menu_slug(), [ static::class, 'get_field_query' ], 10, 2 );
	}

	public static function get_setting($key) {
        $options = static::get_options();
        return $options[$key];
    }

    public static function get_parent_menu_slug() {
	    return 'options-general.php';
    }

	public static function init() {
        add_submenu_page(
            static::get_parent_menu_slug(),
			static::get_title(),
			static::get_title(),
			'manage_options',
			static::get_menu_slug(),
			[ static::class, 'render_settings_page_content' ]
		);
	}

	public static function get_field_query( $result, $key ) {

		$fields = static::get_fields();
		if($fields[$key]) {
			return $fields[$key]['query'];
		}
		return $result;

	}

	public static function get_options() {
		$option_key = static::get_setting_key();
		if($option_key == null) { // Null means we just save directly into options table
			$opts = [];
			$fields = array_keys(static::get_fields());
			foreach($fields as $field) {
				$opts[$field] = get_option($field);
			}
			return $opts;
		}
		return get_option($option_key) ?: [];
	}

	public static function save_options($options) {
		$option_key = static::get_setting_key();
		if($option_key == null) { // Null means we just save directly into options table
			$fields = array_keys(static::get_fields());
			foreach($fields as $field) {
				update_option($field, $options[$field], false);
			}
		} else {
			update_option($option_key, $options, false);
		}
	}

	public static function get_settings() {
		$options = static::get_options();
		return new Settings_Fields(static::get_fields(), $options);
	}

	public static function render_settings_page_content() {
		echo '<div class="wrap" style="max-width: 50rem"><form method="post">';

		echo '<h1>' . esc_html(static::get_title()) . '</h1>';

		PluginCore::add_admin_css();
		PluginCore::add_admin_js();

		$settings = static::get_settings();

		if($_POST) {
			$options = $settings->get_values($_POST);
			static::save_options($options);
			echo '<div class="notice notice-success"><p>' . __('Settings Saved') . '</p></div>';
		}

		$settings->set_auth_path('settings_' . static::get_menu_slug());
		echo '<div class="plugincore-settings-wrapper">';
		$settings->generate_settings_html();
		echo '</div>';

		submit_button();

		echo '</form></div>';
	}

}
