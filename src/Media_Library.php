<?php

namespace InvisibleDragon\PluginCore;

class Media_Library {

	public static function upload_file( $contents, $orig_filename, $private = false ) {

		$upload_dir = wp_upload_dir();

		if($private) {
			$upload_dir = PluginCore::upload_dir($upload_dir, true);
		}

		$filename = wp_unique_filename($upload_dir['path'], $orig_filename);

		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		file_put_contents( $file, $contents );

		$wp_filetype = wp_check_filetype( $filename, null );
		if($private) {
			$wp_filetype['type'] = 'private/file';
		}

		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => sanitize_file_name( $orig_filename ),
			'post_content' => '',
			'post_status' => 'inherit'
		);

		$attach_id = wp_insert_attachment( $attachment, $file );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		return [$file, $attach_id];

	}

}
