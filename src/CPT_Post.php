<?php

namespace InvisibleDragon\PluginCore;

/***
 * This class defines a custom post type for WordPress
 *
 * Make sure activate() is called or it won't be registered
 *
 * @package InvisibleDragon\PluginCore
 */
abstract class CPT_Post {

	/**
	 * The internal name for the post type
	 * @return string
	 */
	abstract public static function get_post_type(): string;

	/**
	 * The label for the post type
	 * @return string
	 */
	abstract public static function get_name(): string;

	public static function get_plural_name() {
		$type = static::get_name();
		if ( substr( $type, -1, 1 ) === 'y' ) {
			$plural_type = substr( $type, 0, -1 ) . 'ies';
		}
		else {
			$plural_type = $type . 's';
		}
		return $plural_type;
	}

	public const FEATURE_TITLE = 'title';
	public const FEATURE_EDITOR = 'editor';
	public const FEATURE_FEATURED_IMAGE = 'thumbnail';
	public const FEATURE_BLOCK_EDITOR = 'block-editor';
	public const FEATURE_AUTHOR = 'author';
	public const FEATURE_PAGE_ATTRIBUTES = 'page-attributes';
	public const FEATURE_REVISIONS = 'revisions';

	public const COLUMN_CHECKBOX = '<input type="checkbox" />';

	private static $_column_cache = [];

	public static function get_supports() {
		return [ static::FEATURE_TITLE, static::FEATURE_EDITOR ];
	}

	static function _default_parent_menu_item() {
		return 'edit.php?post_type=' . static::get_post_type();
	}

	/**
	 * This allows you to pick a different item for this post type to sit under
	 * @return string|null
	 */
	public static function get_parent_menu_item() {
		return static::_default_parent_menu_item();
	}

	public static function get_labels()
	{

		$type = static::get_name();
		$plural_type = static::get_plural_name();

		return array( 'name' => __( "{$plural_type}" ), 'singular_name' => __( $type ),
			'add_new' => __( "Add {$type}" ), 'add_new_item' => __( "Add New {$type}" ),
			'edit_item' => __( "Edit {$type}" ), 'new_item' => __( "New {$type}" ), 'view_item' => __( "View {$type}" ),
			'search_items' => __( "Search {$plural_type}" ), 'not_found' => __( "No {$plural_type} found" ),
			'not_found_in_trash' => __( "No {$plural_type} found in Trash" ), 'parent_item_colon' => '' );

	}

	public static function get_show_in_rest()
	{

		return in_array(static::FEATURE_BLOCK_EDITOR, static::get_supports());

	}

	public static function get_is_public()
	{
		return false;
	}

	public static function get_has_archive()
	{
		return false;
	}

	/**
	 * Overridable function as to whether this post type is hierarchical (can have child posts).
	 *
	 * @return boolean - Whether this post type is hierarchical
	 */
	public static function get_is_hierarchical()
	{

		return false;

	}

	public static function get_icon() {
		return 'dashicons-admin-plugins';
	}

	/**
	 * Get the arguments for this post type to be registered. By default other
	 * functions add information into this array
	 *
	 * @return array
	 */
	public static function get_args() {
		return apply_filters( 'cpt_' . static::get_post_type() . '_args', array(
			'label'                 => static::get_name(),
			'labels'                => static::get_labels(),
			'supports'              => static::get_supports(),
			'show_in_rest'          => static::get_show_in_rest(),
			'has_archive'           => static::get_has_archive(),
			'taxonomies'            => array(),
			'menu_icon'             => static::get_icon(),
			'capability_type'       => 'page',
			'hierarchical'          => static::get_is_hierarchical(),
			'public'                => static::get_is_public(),
			'show_ui'               => true
		));
	}

	/**
	 * Register the post type into WordPress
	 */
	public static function activate() {

		$args = static::get_args();
		register_post_type( static::get_post_type(), $args );

		// Activate metabox
		static::activate_meta();

		// If non-default parent menu item, let us change it!
		if(static::get_parent_menu_item() != static::_default_parent_menu_item()) {
			static::activate_non_default_parent_menu_item();
		}

		add_action( 'admin_menu', [ static::class, 'admin_menu' ] );

		// Activate column changes
		add_filter( 'manage_' . static::get_post_type() . '_posts_columns', [ static::class, 'get_columns' ] );
		add_filter( 'manage_' . static::get_post_type() . '_posts_custom_column', [ static::class, 'get_custom_column' ], 10, 2 );

		add_filter( 'template_include', [ static::class, 'template_include' ] );

		if(is_admin() && isset($_GET['post_type']) && ($_GET['post_type'] ?? null) == static::get_post_type()) {
			add_filter( 'post_row_actions', [ static::class, 'post_row_actions' ], 10, 2 );
		}

	}

	public static function post_row_actions( $actions, $post ) {
		return $actions;
	}

	public static function template_include($template) {

		if(is_singular(static::get_post_type()))
		{
			$try = PluginCore::get_class()::locate_template('single-' . static::get_post_type() . '.php');
			if(file_exists($try)) {
				return $try;
			}
		}

		return $template;
	}

	public static function get_custom_column( $column_key, $post_id ) {

		if(stripos($column_key, 'custom') === 0) {
			$key = substr($column_key, 7);
			$value = get_post_meta($post_id, $key, true);
			if ( is_callable( array( static::class, 'get_' . $key . '_field' ) ) ) {
				if( $value !== '' && static::$_column_cache[ $key ][ $value ] ) {
					echo static::$_column_cache[ $key ][ $value ];
				} else {
					$formatted = call_user_func([static::class, 'get_' . $key . '_field'], $key, $value, $post_id);
					static::$_column_cache[$key][$value] = $formatted;
					echo $formatted;
				}
			} else {
				echo esc_html($value);
			}
		}

	}

	public static function get_columns() {

		return [
			'cb' => static::COLUMN_CHECKBOX,
			'title' => __('Title'),
			'date' => __('Date')
		];

	}

	public static function activate_non_default_parent_menu_item() {
		add_action( 'admin_head', [ static::class, 'choose_parent_file' ] );
		add_action( 'admin_menu', [ static::class, 'admin_menu' ] );
	}

	/**
	 * Override if this post type should have the "Add X" submenu item.
	 * The button to direct users to create a new item will still be available on the listing page
	 * @return bool
	 */
	public static function has_add_submenu_item() {
		return true;
	}

	public static function admin_menu() {

		global $menu;
		global $submenu;

		// Remove top-level menu item
		if(static::get_parent_menu_item() != static::_default_parent_menu_item()) {
			foreach ($menu as $i => $item) {
				if ($item[2] == static::_default_parent_menu_item()) {
					unset($menu[$i]);
				}
			}
		}

		if(!static::has_add_submenu_item()) {
			foreach($submenu[ static::_default_parent_menu_item() ] as $k => $item ) {
				if(stripos( $item[2], 'post-new.php?post_type=' . static::get_post_type() ) !== false ) {
					unset($submenu[ static::_default_parent_menu_item() ][ $k ]);
				}
			}
		}

		// Put the contents under the other one
		if(static::get_parent_menu_item() != static::_default_parent_menu_item()) {
			$submenu[static::get_parent_menu_item()] = array_merge(
				$submenu[static::get_parent_menu_item()] ?: [],
				$submenu[static::_default_parent_menu_item()] ?: []
			);
		}

	}

	public static function choose_parent_file() {
		global $parent_file;

		if($parent_file == 'edit.php?post_type=' . static::get_post_type()) {
			$parent_file = static::get_parent_menu_item();
		}
	}

	public static function activate_meta() {

		add_action( 'add_meta_boxes_' . static::get_post_type(), [ static::class, 'add_meta_boxes' ] );
		add_filter( 'plugincore_query_cpt_' . static::get_post_type(), [ static::class, 'get_field_query' ], 10, 2 );
		add_action( 'save_post', [ static::class, 'save_fields' ] );

	}

	/**
	 * Return custom post statuses with a subclass if you wish to use these instead
	 * of the default WordPress provided
	 * @return null|array
	 */
	public static function get_custom_post_statuses() {
		return null;
	}

	public static function save_fields( $post_id ) {

		// Verification of post
		if(!isset($_POST['metabox_' . static::get_post_type() . '_nonce'])) return $post_id;
		$nonce = $_POST['metabox_' . static::get_post_type() . '_nonce'];
		if(!wp_verify_nonce($nonce, 'metabox_' . static::get_post_type() . '_fields')) return $post_id;

		// Skip on autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		// Now actually set the metadata
		$tabs = static::get_all_tabs();

		$settings = Settings_Fields::from_tabs($tabs, $_POST, 'meta');
		$values = $settings->get_values($_POST);

		foreach($values as $key => $value) {
			update_post_meta($post_id, $key, $value);
		}

		return $post_id;

	}

	public static function get_field_query( $result, $key ) {

		$tabs = static::get_all_tabs();
		foreach($tabs as $tab) {
            $try = Settings_Fields::find_field($tab['fields'], $key);
            if($try) {
                return $try['query'];
            }
		}
		return $result;

	}

	/**
	 * @return string Should be one of 'high', 'core', 'default', or 'low'.
	 */
	public static function get_meta_box_priority() {
		return 'default';
	}

	public static function add_meta_boxes() {
		global $wp_meta_boxes;
		add_meta_box(
			'cpt_post_data_' . static::get_post_type(),
			static::get_name(),
			[ static::class, 'meta_box_callback' ],
			static::get_post_type(),
			'normal',
			static::get_meta_box_priority()
		);
		add_filter( 'postbox_classes_' . static::get_post_type() . '_cpt_post_data_' . static::get_post_type(), [ static::class, 'add_metabox_class' ] );

		if(static::get_custom_post_statuses()) {
			remove_meta_box('submitdiv', null, 'side');
			add_meta_box( 'customsubmit', __( 'Status' ), [ static::class, 'publish_meta_box' ], static::get_post_type(), 'side', 'high' );
		}

	}

	public static function publish_meta_box($post) {
		require_once dirname(__FILE__) . '/../templates/custom-publish-box.php';
	}

	public static function add_metabox_class($classes) {
		$classes[] = 'plugincore-metabox';
		return $classes;
	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => 'General',
				'fields' => [
					'title' => array(
						'title' => 'Example',
						'type' => 'text',
						'description' => 'This is an example',
						'default' => 'Cheese'
					)
				]
			]
		];
	}

	public static function get_all_tabs() {
		return apply_filters( 'cpt_' . static::get_post_type() . '_tabs', static::get_tabs() );
	}

	public static function meta_box_callback($post) {

		$tabs = static::get_all_tabs();
		$raw_values = get_post_meta($post->ID);
		$values = [];
		foreach($raw_values as $key => $r) {
			$values[$key] = maybe_unserialize($r[0]);
		}

		PluginCore::add_admin_css();
		PluginCore::add_admin_js();

		wp_nonce_field( 'metabox_' . static::get_post_type() . '_fields', 'metabox_' . static::get_post_type() . '_nonce' );

		$settings = Settings_Fields::from_tabs($tabs, $values, 'meta');
		$settings->set_auth_path('cpt_' . static::get_post_type());
		$settings->generate_settings_html();

		echo '<div class="clear"></div>';

	}

}
