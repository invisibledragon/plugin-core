<?php

namespace InvisibleDragon\PluginCore;

/**
 * This class defines a custom taxonomy for WordPress
 *
 * Make sure activate() is called or it won't be registered
 *
 * @package InvisibleDragon\PluginCore
 */
abstract class Taxonomy {

    public abstract static function get_handle();
    public abstract static function get_name();
    public abstract static function get_post_types();

    public static function get_is_hierarchical() {
        return false;
    }

    public static function get_is_public() {
        return false;
    }

    public static function get_settings() {
        return [];
    }

    /**
     * Get the arguments for this taxonomy to be registered. By default other
     * functions add information into this array
     *
     * @return array
     */
    public static function get_args() {
        return apply_filters( 'tax_' . static::get_handle() . '_args', array(
            'label'                 => static::get_name(),
            'hierarchical'          => static::get_is_hierarchical(),
            'public'                => static::get_is_public(),
            'show_ui'               => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
        ));
    }

    /**
     * Register the taxonomy into WordPress
     */
    public static function activate() {

        $args = static::get_args();
        register_taxonomy( static::get_handle(), static::get_post_types(), $args );

        if(is_admin() && $_GET['taxonomy'] == static::get_handle()) {
            add_filter( 'bulk_actions-edit-' . static::get_handle(), [ static::class, 'bulk_actions' ] );
            add_filter( 'handle_bulk_actions-edit-' . static::get_handle(), [ static::class, '_bulk_action' ], 10, 3 );
            add_filter( static::get_handle() . '_row_actions', [ static::class, 'post_row_actions' ], 10, 2 );
        }

        add_action( static::get_handle() . '_add_form_fields', [ static::class, 'output_edit_fields' ] );
        add_action( static::get_handle() . '_edit_form', [ static::class, 'output_edit_fields' ] );
        add_action( 'edit_' . static::get_handle(), [ static::class, 'save_edit_fields' ] );
        add_action( 'create_' . static::get_handle(), [ static::class, 'save_edit_fields' ] );
        add_filter( 'plugincore_query_tax_' . static::get_handle(), [ static::class, 'get_field_query' ], 10, 2 );
		add_filter( 'plugincore_fields_taxonomy_' . static::get_handle(), [ static::class, 'get_settings' ] );

    }

    public static function get_field_query( $result, $key ) {

        $settings = static::get_settings();
        return $settings[$key]['query'];

    }

    public static function save_edit_fields($term_id) {

        if(!isset($_POST['metabox_' . static::get_handle() . '_nonce'])) return;
        $nonce = $_POST['metabox_' . static::get_handle() . '_nonce'];
        if(!wp_verify_nonce($nonce, 'metabox_' . static::get_handle() . '_fields')) return;

        $s = static::get_settings();
        $settings = new Settings_Fields($s, $_POST, 'tax');
        $values = $settings->get_values($_POST);

        foreach($values as $key => $value) {
            update_term_meta($term_id, $key, $value);
        }

    }

    public static function output_edit_fields($tag) {
        $s = static::get_settings();
        if(!empty($s)) {
            PluginCore::add_admin_css();
            PluginCore::add_admin_js();

            $values = [];
            if(is_object($tag)) {
                $meta = get_term_meta($tag->term_id);
                foreach($meta as $key => $r) {
                    $values[$key] = maybe_unserialize($r[0]);
                }
            }

            wp_nonce_field( 'metabox_' . static::get_handle() . '_fields', 'metabox_' . static::get_handle() . '_nonce' );

            $settings = new Settings_Fields($s, $values, 'tax');
            $settings->set_auth_path('tax_' . static::get_handle());
            $settings->generate_settings_html();

            echo '<div class="clear"></div>';
        }
    }

    public static function post_row_actions( $actions, $post ) {
        return $actions;
    }

    public static function bulk_actions($actions) {
        return $actions; // Pass-through as a default
    }

    public static function bulk_action( $redirect, $doaction, $object_ids ) {
        return $redirect; // Pass through as a default
    }

    public static function _bulk_action( $redirect, $doaction, $object_ids ) {
        $actions = static::bulk_actions( $actions );
        $redirect = remove_query_arg( array_keys( $actions ), $redirect );
        return static::bulk_action( $redirect, $doaction, $object_ids );
    }

}
