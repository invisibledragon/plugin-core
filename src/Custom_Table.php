<?php

namespace InvisibleDragon\PluginCore;

/**
 * Custom Table which will be added to the database, with simple WordPress Admin
 */
abstract class Custom_Table {

    private static $_column_cache = [];

    /**
     * Return the table name
     * @return string
     */
    public abstract static function get_table_name() : string;

    /**
     * Return the create table sql
     * @return string
     */
    public abstract static function get_create_table_sql() : string;

    /**
     * Returns a label for the table to be used in the WP UI
     * @return string
     */
    public abstract static function get_label() : string;

    /**
     * Return a list of columns. Key is column name => Label for column
     * @return array
     */
    public abstract static function get_columns() : array;

    static function _default_parent_menu_item() {
        return 'lst_' . static::get_table_name();
    }

    public static function has_add_ui() {
        return true;
    }

    /**
     * This allows you to pick a different item for this post type to sit under
     * @return string|null
     */
    public static function get_parent_menu_item() {
        return static::_default_parent_menu_item();
    }

    public static function activate() {

        add_action( 'upgrader_process_complete', [ static::class, 'install_database_table' ] );
        if(isset($_GET['__install_db_tables'])) {
            static::install_database_table();
        }

        add_action( 'admin_menu', [ static::class, 'admin_menu' ], 9 );
        add_filter( 'plugincore_query_ct_edit_' . static::get_table_name(), [ static::class, 'get_field_query' ], 10, 2 );

    }

    public static function get_field_query( $result, $key ) {

        $tabs = static::get_tabs();
        foreach($tabs as $tab) {
            $try = Settings_Fields::find_field($tab['fields'], $key);
            if($try) {
                return $try['query'];
            }
        }
        return $result;

    }

    public abstract static function get_default_order() : array;

    public static function get_custom_column( $key, $item ) {

        if($key == 'edit') {
            echo '<a href="' . admin_url('admin.php?page=add_' . static::get_table_name() . '&id=' . $item['id']) . '">';
            echo __('Edit');
            echo '</a>';
            return;
        }

        $value = $item[$key];
        if ( is_callable( array( static::class, 'get_' . $key . '_field' ) ) ) {
            if( $value !== '' && @static::$_column_cache[ $key ][ $value ] ) {
                echo static::$_column_cache[ $key ][ $value ];
            } else {
                $formatted = call_user_func([static::class, 'get_' . $key . '_field'], $key, $value, $item);
                static::$_column_cache[$key][$value] = $formatted;
                echo $formatted;
            }
        } else {
            echo esc_html($value);
        }

    }

    public static function update($item_id, $items) {
        global $wpdb;
        $r = $wpdb->update( static::get_table_name(), $items, [ 'id' => $item_id ] );
        if($r === false) {
            throw new \Exception("Could not update " . static::get_table_name() . ': ' . $wpdb->last_error );
        }
    }

    public static function insert($item) {
        global $wpdb;
        $r = $wpdb->insert( static::get_table_name(), $item );
        if($r === false) {
            throw new \Exception("Could not insert " . static::get_table_name() . ': ' . $wpdb->last_error );
        }
        return $wpdb->insert_id;
    }

    public static function get_item($id) {
        global $wpdb;
        $sql = 'SELECT * FROM ' . static::get_table_name() . ' WHERE id = ' . intval($id);
        return $wpdb->get_row( $sql, ARRAY_A );
    }

    public static function get_items($where = null, $order = null, $limits = null) {
        global $wpdb;
        $sql = 'SELECT * FROM ' . static::get_table_name() . ' WHERE 1=1 ';
	    foreach($where ?: [] as $cond) {
		    $sql .= ' AND `' . $cond['key'] . '` ';
		    $has_value = true;
		    switch($cond['compare']) {
			    case 'IS NULL':
				    $sql .= ' IS NULL ';
				    $has_value = false;
				    break;
			    case 'IS NOT NULL':
				    $sql .= ' IS NOT NULL ';
				    $has_value = false;
				    break;
			    case '=':
				    $sql .= ' = ';
				    break;
			    default:
				    throw new \Exception('Cannot query compare type ' . $cond['compare']);
		    }
		    if($has_value) {
			    $sql .= $wpdb->prepare( '%s', $cond['value'] );
		    }
	    }
        if($order == null) {
            $order = static::get_default_order();
        }
        $sql .= ' ORDER BY ' . implode(' ', $order);
        if($limits != null) {
            $sql .= ' LIMIT ' . $limits[0] . ' OFFSET ' . $limits[1];
        }
        return $wpdb->get_results( $sql, ARRAY_A );
    }

    public static function get_menu_icon() {
        return 'dashicons-admin-post';
    }

    public static function get_menu_position() {
        return 30;
    }

    public static function admin_menu() {
        // If non-default parent menu item, let us change it!
        if(static::get_parent_menu_item() != static::_default_parent_menu_item()) {
            add_submenu_page(
                static::get_parent_menu_item(),
                static::get_label(),
                static::get_label(),
                'edit_posts', // TODO: Permissions
                'lst_' . static::get_table_name(),
                [ static::class, 'list_table_page' ]
            );
        } else {
            add_menu_page(
                static::get_label(),
                static::get_label(),
                'edit_posts', // TODO: Permissions
                'lst_' . static::get_table_name(),
                [ static::class, 'list_table_page' ],
                static::get_menu_icon(),
                static::get_menu_position()
            );
        }

        add_submenu_page(
            static::get_parent_menu_item(),
            sprintf(__('Add %s'), static::get_label()),
            sprintf(__('Add %s'), static::get_label()),
            'edit_posts', // TODO: Permissions
            'add_' . static::get_table_name(),
            [ static::class, 'add_edit_page' ]
        );

    }

    /**
     * Admin page for listing items
     * @return void
     */
    public static function list_table_page() {
        require_once dirname(__FILE__) . '/../templates/custom-table-list.php';
    }

    public static function get_tabs() {
        return [];
    }

    public static function get_settings($data) {
        $tabs = static::get_tabs();
        return Settings_Fields::from_tabs($tabs, $data, 'meta');
    }

    /**
     * Admin page for add/editing items
     * @return void
     */
    public static function add_edit_page() {
        if(!isset($_GET['id']) && !static::has_add_ui()) {
            echo __('Adding of items is not available');
            return;
        }

        PluginCore::add_admin_css();
        PluginCore::add_admin_js();

        echo '<div class="wrap" style="max-width: 50rem"><form method="post">';

        echo '<h1>' . esc_html(static::get_label()) . '</h1>';

        PluginCore::add_admin_css();
        PluginCore::add_admin_js();

        if(isset($_GET['id'])) {
            $item = static::get_item( $_GET['id'] );
            if ( ! $item ) {
                wp_die( "Not Found", "Not Found" );
            }
        } else {
            $item = null;
        }
        $settings = static::get_settings($item);

        if($_POST) {
            $options = $settings->get_values($_POST);
            if(isset($_GET['id'])) {
                static::update($_GET['id'], $options);
            } else {
                $id = static::insert($options);
                $new_url = add_query_arg([
                    'id' => $id,
                    'created' => 1
                ]);
                echo '<script>document.location.href=' . json_encode($new_url) . '</script>';
                exit(0);
            }
            echo '<div class="notice notice-success"><p>' . __('Saved') . '</p></div>';
        }
        if(isset($_GET['created'])) {
            echo '<div class="notice notice-success"><p>' . __('Created') . '</p></div>';
        }

        $settings->set_auth_path('ct_edit_' . static::get_table_name());
        echo '<div class="plugincore-settings-wrapper">';
        $settings->generate_settings_html();
        echo '</div>';

        submit_button();

        echo '</form></div>';

    }

    public static function install_database_table() {

        global $wpdb;
        $opt_name = 'tableversion_' . static::get_table_name();
        $opt = get_option( $opt_name );
        if( !$opt ) {

            $result = $wpdb->query(static::get_create_table_sql());
	        if($result === false) {
		        $err = $wpdb->last_error;
		        if(stripos('already exists', $err) !== false) {
			        $result = 1;
		        }
	        }
            if($result === false) {
                echo '<p><strong>Warning: </strong> Failure to install custom table</p>';
            } else {
                update_option( $opt_name, 1 );
            }

        }

    }

}
