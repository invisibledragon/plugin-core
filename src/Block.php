<?php

namespace InvisibleDragon\PluginCore;

abstract class Block {

	public const INNER_BLOCKS_NONE = 'none';
	public const INNER_BLOCKS_TOP = 'top';
	public const INNER_BLOCKS_BOTTOM = 'bottom';
	public const INNER_BLOCKS_RIGHT = 'right';

	/**
	 * Get the name used for this block. This is namespaced so for example `invisibledragon/big-block`
	 * @return string
	 */
	public abstract static function get_name();

	/**
	 * Get the title used for this block (probably want to translate this!)
	 * @return string
	 */
	public abstract static function get_title();

	/**
	 * Returns fields for the main block settings to be displayed
	 * @return array
	 */
	public abstract static function get_fields();

	/**
	 * Where should inner blocks be displayed inside of the block editor?
	 * @return string
	 */
	public static function get_inner_blocks_position() {
		return static::INNER_BLOCKS_NONE;
	}

	/**
	 * Return settings for inner blocks, for example:
	 * * orientation => "horizontal"
	 * * allowedBlocks => [ "core/text", "other-block" ]
	 * @return array
	 */
	public static function get_inner_blocks_settings() {
		return [];
	}

	/**
	 * Overridable function for a category. You will need to register the category using the
	 * WordPress`block_categories` hook
	 * @return string
	 */
	public static function get_category() {
		return 'text';
	}

	/**
	 * Returns an array of arguments which can be passed to create the block.
	 *
	 * Follows Block API for registration -
	 * https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
	 * @return array
	 */
	public static function get_args() {
		$fields = static::get_fields();

		foreach($fields as &$field) {
			// todo Make this work in settings_field as well
			if($field['type'] == 'select') {
				if(is_callable( $field['options'] )) {
					$field['options'] = call_user_func( $field['options'] );
				}
			}
		}

		return [
			'$schema' => "https://json.schemastore.org/block.json",
			"apiVersion" => 2,
			'name' => static::get_name(),
			'title' => static::get_title(),
			"category" => static::get_category(),
			"render_callback" => [ static::class, 'render_block' ],
			"fields" => $fields,
			"settings" => [
				"inner_blocks" => static::get_inner_blocks_position(),
				"inner_block_settings" => static::get_inner_blocks_settings()
			]
		];
	}

	public static abstract function render($fields, $attributes, $content);

	public static function render_block( $attributes, $content ) {
		$fields = $attributes['data'] ?? [];

		// Unset the "data" property because we
		// pass it as separate argument to the callback.
		unset($attributes['data']);

		ob_start();

		call_user_func( [ static::class, 'render' ] , $fields, $attributes, $content );

		return ob_get_clean();
	}

	public static function activate() {

		PC_Settings_JS::add_block(static::class);

	}

}
