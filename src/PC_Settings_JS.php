<?php

namespace InvisibleDragon\PluginCore;

/**
 * This class looks after the React bridge between PluginCore and the React things
 */
class PC_Settings_JS {

	private static $_block_directory = [];
	private static $this_instance = '__pcPrefix';
	private static $_asset_file;

	public static function add_block($blk) {
		if (empty(static::$_block_directory)) {
			add_action('admin_init', [static::class, 'enqueue_scripts']);
		}

		$item = $blk::get_args();
		static::$_block_directory[] = $item;

		register_block_type( $item['name'], array(
			'editor_script' => 'plugincore-block-' . static::$this_instance,
			'render_callback' => $item['render_callback']
		));
	}

	public static function get_prefix() {
		static::get_asset_file();
		return static::$this_instance;
	}

	public static function get_asset_file() {
		if(!static::$_asset_file) {
			// automatically load dependencies and version
			static::$_asset_file = include( dirname( __FILE__ ) . '/../assets/jsx/index.asset.php');

			// If we've been installed and configured, we'll have one of these!
			if(static::$_asset_file['prefix'] ?? false) {
				static::$this_instance = static::$_asset_file['prefix'];
			}
		}
	}

    public static function get_toolbars() {

        // vars
        $editor_id = 'plugincore_editor';
        $toolbars  = array();

        // mce buttons (Full)
        $mce_buttons   = array( 'formatselect', 'bold', 'italic', 'bullist', 'numlist', 'blockquote', 'alignleft', 'aligncenter', 'alignright', 'link', 'wp_more', 'spellchecker', 'fullscreen', 'wp_adv' );
        $mce_buttons_2 = array( 'strikethrough', 'hr', 'forecolor', 'pastetext', 'removeformat', 'charmap', 'outdent', 'indent', 'undo', 'redo', 'wp_help' );

        // mce buttons (Basic)
        $teeny_mce_buttons = array( 'bold', 'italic', 'underline', 'blockquote', 'strikethrough', 'bullist', 'numlist', 'alignleft', 'aligncenter', 'alignright', 'undo', 'redo', 'link', 'fullscreen' );

        // Full
        $toolbars['Full'] = array(
            1 => apply_filters( 'mce_buttons', $mce_buttons, $editor_id ),
            2 => apply_filters( 'mce_buttons_2', $mce_buttons_2, $editor_id ),
            3 => apply_filters( 'mce_buttons_3', array(), $editor_id ),
            4 => apply_filters( 'mce_buttons_4', array(), $editor_id ),
        );

        // Basic
        $toolbars['Basic'] = array(
            1 => apply_filters( 'teeny_mce_buttons', $teeny_mce_buttons, $editor_id ),
        );

        // Filter for 3rd party
        $toolbars = apply_filters( 'plugincore_tinymce_toolbars', $toolbars );

        // return
        return $toolbars;

    }

	public static function enqueue_scripts() {

		static::get_asset_file();
		$asset_file = static::$_asset_file;

		PluginCore::add_admin_css();

		wp_register_script(
			'plugincore-' . static::$this_instance,
			PluginCore::get_plugincore_dir() . '/assets/jsx/index.js',
			$asset_file['dependencies'],
			$asset_file['version']
		);

		wp_localize_script(
			'plugincore-' . static::$this_instance,
			'plugincore_' . static::$this_instance,
			array(
				'blocks' => static::$_block_directory,
                'tinymceScriptSrc' => includes_url( 'js/tinymce/' ) . 'wp-tinymce.js',
                'tinymceToolbars' => static::get_toolbars(),
			)
		);

		wp_enqueue_script('plugincore-' . static::$this_instance);
		wp_enqueue_style( 'wp-components' );

		do_action( 'plugincore_enqueue_scripts', static::$this_instance );

        add_action( 'wp_footer', [ static::class, 'footer' ] );
        add_action( 'admin_footer', [ static::class, 'footer' ] );

	}

    public static function footer() {
        // Tells WordPress to output all of it's tinymce bits and pieces
        ?>
        <div id="plugincore-hidden-wp-editor" style="display: none;">
            <?php wp_editor( '', 'plugincore_tinymce' ); ?>
        </div>
        <?php
    }

}
