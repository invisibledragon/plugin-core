<?php

namespace InvisibleDragon\PluginCore;

abstract class User_Settings {

	public static abstract function get_fields();

	public static abstract function get_label();

	public static abstract function get_key();

	public static function activate() {
		add_action( 'edit_user_profile', [ static::class, 'user_profile' ] );
		add_action( 'show_user_profile', [ static::class, 'user_profile' ] );

		add_action( 'personal_options_update', [ static::class, 'save_profile' ] );
		add_action( 'edit_user_profile_update', [ static::class, 'save_profile' ] );

		add_filter( 'plugincore_query_up_' . static::get_key(), [ static::class, 'get_field_query' ], 10, 2 );
	}

	public static function save_profile($user_id) {

		if(is_object($user_id)) {
			$user_id = $user_id->ID;
		}

		if(!isset($_POST[static::get_key() . '_nonce'])) return;
		$nonce = $_POST[static::get_key() . '_nonce'];
		if(!wp_verify_nonce($nonce, static::get_key() . '_fields')) return;

		$settings = new Settings_Fields( static::get_fields(), $_POST, 'up_' . static::get_key() );
		$values = $settings->get_values($_POST);

		foreach($values as $key => $value) {
			update_user_meta($user_id, $key, $value);
		}
	}

	public static function get_field_query( $result, $key ) {

		$fields = static::get_fields();
		return $fields[$key]['query'];

	}

	public static function get_values($user_id) {
		$raw_values = get_user_meta($user_id);
		$values = [];
		foreach($raw_values as $key => $r) {
			$values[$key] = maybe_unserialize($r[0]);
		}
		return $values;
	}

	public static function user_profile($user_id) {

		if(is_object($user_id)) {
			$user_id = $user_id->ID;
		}

		echo '<h2>' . esc_html(static::get_label()) . '</h2>';

		wp_nonce_field( static::get_key() . '_fields', static::get_key() . '_nonce' );

		$settings = new Settings_Fields( static::get_fields(), static::get_values($user_id), 'up_' . static::get_key() );
		$settings->set_auth_path('up_' . static::get_key());
		$settings->generate_settings_html();

	}

}
