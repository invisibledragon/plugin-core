<?php

namespace InvisibleDragon\PluginCore;

class Taxonomy_Helper {

	private static $_cache = [];

	/**
	 * For a given term, return an array of strings of parent term names
	 *
	 * Result may be cached in memory
	 *
	 * @param \WP_Term $term
	 * @return array
	 */
	public static function get_hierarchical_term_prefix( $term ) {

		$parent_id = $term->parent;
		if(isset(static::$_cache[$parent_id])) {
			return static::$_cache[ $parent_id ];
		}

		$orig_parent_id = $parent_id;
		$output = [];
		while($parent_id != 0){
			$term = get_term_by( 'term_id', $parent_id, $term->taxonomy );
			$output[] = $term->name;
			$parent_id = $term->parent;
		}

		static::$_cache[$orig_parent_id] = $output;
		return $output;

	}

}
