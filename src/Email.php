<?php

namespace InvisibleDragon\PluginCore;

/**
 * Base class for an email template to be configured and sent out
 *
 * Email template included is based on the WooCommerce template as it's pretty neat
 */
abstract class Email {

	private $_settings = [];

	/**
	 *  List of preg* regular expression patterns to search for,
	 *  used in conjunction with $plain_replace.
	 *  https://raw.github.com/ushahidi/wp-silcc/master/class.html2text.inc
	 *
	 *  @var array $plain_search
	 *  @see $plain_replace
	 */
	public $plain_search = array(
		"/\r/",                                                  // Non-legal carriage return.
		'/&(nbsp|#0*160);/i',                                    // Non-breaking space.
		'/&(quot|rdquo|ldquo|#0*8220|#0*8221|#0*147|#0*148);/i', // Double quotes.
		'/&(apos|rsquo|lsquo|#0*8216|#0*8217);/i',               // Single quotes.
		'/&gt;/i',                                               // Greater-than.
		'/&lt;/i',                                               // Less-than.
		'/&#0*38;/i',                                            // Ampersand.
		'/&amp;/i',                                              // Ampersand.
		'/&(copy|#0*169);/i',                                    // Copyright.
		'/&(trade|#0*8482|#0*153);/i',                           // Trademark.
		'/&(reg|#0*174);/i',                                     // Registered.
		'/&(mdash|#0*151|#0*8212);/i',                           // mdash.
		'/&(ndash|minus|#0*8211|#0*8722);/i',                    // ndash.
		'/&(bull|#0*149|#0*8226);/i',                            // Bullet.
		'/&(pound|#0*163);/i',                                   // Pound sign.
		'/&(euro|#0*8364);/i',                                   // Euro sign.
		'/&(dollar|#0*36);/i',                                   // Dollar sign.
		'/&[^&\s;]+;/i',                                         // Unknown/unhandled entities.
		'/[ ]{2,}/',                                             // Runs of spaces, post-handling.
	);

	/**
	 *  List of pattern replacements corresponding to patterns searched.
	 *
	 *  @var array $plain_replace
	 *  @see $plain_search
	 */
	public $plain_replace = array(
		'',                                             // Non-legal carriage return.
		' ',                                            // Non-breaking space.
		'"',                                            // Double quotes.
		"'",                                            // Single quotes.
		'>',                                            // Greater-than.
		'<',                                            // Less-than.
		'&',                                            // Ampersand.
		'&',                                            // Ampersand.
		'(c)',                                          // Copyright.
		'(tm)',                                         // Trademark.
		'(R)',                                          // Registered.
		'--',                                           // mdash.
		'-',                                            // ndash.
		'*',                                            // Bullet.
		'£',                                            // Pound sign.
		'EUR',                                          // Euro sign. € ?.
		'$',                                            // Dollar sign.
		'',                                             // Unknown/unhandled entities.
		' ',                                             // Runs of spaces, post-handling.
	);

	/**
	 * Return the name of this email (used for settings, and hooks etc)
	 * @return string
	 */
	public abstract static function get_key();

	public abstract static function get_title();
	public abstract static function get_description();

	/**
	 * Should this type of email have a direct recipient? i.e true if this goes direct to a customer
	 * @return boolean
	 */
	public static function has_direct_recipient() {
		return false;
	}

	public static function get_setting_field() {
		$recipient = null;
		if(!static::has_direct_recipient()) {
			$recipient = [
				'type' => 'text',
				'title' => __('Recipient'),
				'default' => get_option( 'admin_email' )
			];
		}

		return [
			'type' => 'modal',
			'title' => static::get_title(),
			'description' => static::get_description(),
			'fields' => array_filter([
				'enabled' => [
					'type' => 'checkbox',
					'title' => __('Enabled'),
					'default' => true
				],
				'email_type' => [
					'type' => 'select',
					'title' => __('Email Type'),
					'options' => [
						'html' => __('HTML'),
						'plain' => __('Plain Text')
					],
					'default' => 'html'
				],
				'recipient' => $recipient,
				'title' => [
					'type' => 'text',
					'title' => __('Title'),
				],
				'body' => [
					'type' => 'textarea',
					'title' => __('Body')
				]
			])
		];
	}

	/**
	 * Returns the settings stored using the setting fields defined
	 * @return array
	 */
	public static function get_settings() {
		return get_option(static::get_key());
	}

	public function __construct() {
		$this->_settings = static::get_settings();
	}

	/**
	 * Returns the heading to be used by this email
	 * @return string
	 */
	public abstract function get_default_heading();

	public function get_heading() {
		return $this->format_string($this->_settings['title'] ?: $this->get_default_heading());
	}

	public function get_default_body() {
		return '';
	}

	public function get_body() {
		return $this->format_string($this->_settings['body'] ?: $this->get_default_body());
	}

	/**
	 * Return the HTML version of this email to be sent
	 * @return string
	 */
	public abstract function get_content_html();

	/**
	 * Return the plain text version of this email to be sent
	 * @return string
	 */
	public abstract function get_content_plain();

	/**
	 * Provide any key-value placeholders to be used within email subjects etc
	 * @return array
	 */
	public function get_placeholders() {
		return [];
	}

	/**
	 * Return the intended recipient. Defaults to getting the site admin
	 * @return string|string[]
	 */
	public function get_recipient() {
		return $this->_settings['recipient'] ?: get_option( 'admin_email' );
	}

	/**
	 * Returns the email subject line. Defaults to heading
	 * @return string
	 */
	public function get_subject() {
		return $this->get_heading();
	}

	/**
	 * Returns if this email should be sent as plain text, or as HTML
	 * @return string
	 */
	public function get_email_type() {
		return class_exists( 'DOMDocument' ) ? ($this->_settings['email_type'] ?: 'html') : 'plain';
	}

	/**
	 * Get email content type.
	 *
	 * @return string
	 */
	public function get_content_type() {
		switch ( $this->get_email_type() ) {
			case 'html':
				$content_type = 'text/html';
				break;
			case 'multipart':
				$content_type = 'multipart/alternative';
				break;
			default:
				$content_type = 'text/plain';
				break;
		}
		return $content_type;
	}

	/**
	 * Returns email headers
	 * @return string
	 */
	public function get_headers() {
		$header = 'Content-Type: ' . $this->get_content_type() . "\r\n";
		return $header;
	}

	/**
	 * Returns a list of attachments. Defaults to attaching none
	 * @return array
	 */
	public function get_attachments() {
		return [];
	}

	public function is_enabled() {
		return $this->_settings['enabled'] ?: true;
	}

	public function get_content() {
		$this->sending = true;

		if ( 'plain' === $this->get_email_type() ) {
			$email_content = wordwrap( preg_replace( $this->plain_search, $this->plain_replace, wp_strip_all_tags( $this->get_content_plain() ) ), 70 );
		} else {
			$email_content = $this->get_content_html();
		}

		return $email_content;
	}

	public function style_inline( $content ) {
		if ( in_array( $this->get_content_type(), array( 'text/html', 'multipart/alternative' ), true ) ) {
			ob_start();
			require( dirname(__FILE__) . '/../templates/email-styles.php' );
			$css = apply_filters( 'woocommerce_email_styles', ob_get_clean(), $this );

			$css_inliner_class = CssInliner::class;

			if ( $this->supports_emogrifier() && class_exists( $css_inliner_class ) ) {
				try {
					$css_inliner = CssInliner::fromHtml( $content )->inlineCss( $css );

					do_action( 'woocommerce_emogrifier', $css_inliner, $this );

					$dom_document = $css_inliner->getDomDocument();

					HtmlPruner::fromDomDocument( $dom_document )->removeElementsWithDisplayNone();
					$content = CssToAttributeConverter::fromDomDocument( $dom_document )
						->convertCssToVisualAttributes()
						->render();
				} catch ( Exception $e ) {
					// $logger = wc_get_logger();
					// $logger->error( $e->getMessage(), array( 'source' => 'emogrifier' ) );
				}
			} else {
				$content = '<style type="text/css">' . $css . '</style>' . $content;
			}
		}

		return $content;
	}

	protected function supports_emogrifier() {
		return class_exists( 'DOMDocument' );
	}

	public function get_header() {
		require dirname(__FILE__) . '/../templates/email-header.php';
	}

	public function get_footer() {
		require dirname(__FILE__) . '/../templates/email-footer.php';
	}

	public function format_string($string) {
		$placeholders = $this->get_placeholders();
		$placeholders['{blogname}'] = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		$placeholders['{site_title}'] = $placeholders['{blogname}'];
		$placeholders['{site_address}'] = wp_parse_url( home_url(), PHP_URL_HOST );
		$placeholders['{site_url}'] = wp_parse_url( home_url(), PHP_URL_HOST );
		$search = array_keys($placeholders);
		$values = array_values($placeholders);
		return str_replace($search, $values, $string);
	}

	public function send($force = false) {
		if($force === false && !$this->is_enabled()) {
			return; // Do not actually send it!
		}

		$message = $this->get_content();
		$message = apply_filters( 'woocommerce_mail_content', $this->style_inline( $message ) );
		wp_mail(
			$this->get_recipient(),
			$this->format_string($this->get_subject()),
			$message,
			$this->get_headers(),
			$this->get_attachments()
		);

	}

}
