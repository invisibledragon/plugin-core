<?php

namespace InvisibleDragon\PluginCore;

class CPT_Comment_Sidebar {

	private $_post_type;
	private $_comment_type;

	public static function activate($post_type, $comment_type) {
		new CPT_Comment_Sidebar($post_type, $comment_type);
	}

	public function __construct($post_type, $comment_type) {
		$this->_post_type = $post_type;
		$this->_comment_type = $comment_type;
		add_action( 'add_meta_boxes_' . $this->_post_type, [ $this, 'add_meta_boxes' ] );
		add_action( 'wp_ajax_pcaddnote_' . $this->_post_type, [ $this, 'add_note_ajax' ] );

		$this->add_comment_filters();
	}

	public function add_comment_filters() {
		add_filter( 'comment_feed_where', [ $this, 'exclude_comments_from_feed_where' ] );
		add_filter( 'comments_clauses', [ $this, 'exclude_comments' ], 10, 1 );
	}

	public function exclude_comments( $clauses ) {
		$clauses['where'] .= ( $clauses['where'] ? ' AND ' : '' ) . " comment_type != '" . $this->_comment_type . "' ";
		return $clauses;
	}

	public function exclude_comments_from_feed_where( $where ) {
		return $where . ( $where ? ' AND ' : '' ) . " comment_type != '" . $this->_comment_type . "' ";
	}

	public function get_name() {
		return __('Notes');
	}

	public function add_meta_boxes() {
		add_meta_box(
			'cpt_comment_sidebar_' . $this->_post_type,
			$this->get_name(),
			[ $this, 'meta_box_callback' ],
			$this->_post_type,
			'side',
			'default'
		);
	}

	public function comment_form_extra() {
		// Do nothing here
	}

	public function meta_box_callback() {
		PC_Settings_JS::enqueue_scripts();

		remove_filter( 'comment_feed_where', [ $this, 'exclude_comments_from_feed_where' ] );
		remove_filter( 'comments_clauses', [ $this, 'exclude_comments' ], 10, 1 );

		$comments = get_comments([
			'post_id' => get_the_ID()
		]);

		$this->add_comment_filters();

		echo '<ul class="plugincore-notes-sidebar">';
		foreach($comments as $comment) {
			echo '<li><div class="comment">' . esc_html($comment->comment_content) . '</div>';
			$author = $comment->comment_author;
			if(!$author) {
				$author = get_the_author_meta('user_nicename', $comment->user_id);
			}
			echo '<div class="author">' . esc_html($author) . '</div>';
			echo '<div class="date">';
			comment_date('', $comment);
			echo '</div>';
			echo '</li>';
		}
		echo '</ul>';
		echo '<label for="new_note">' . __('Add Note') . '</label>';
		echo '<textarea class="plugincore-notes-new" id="new_note" name="new_note" form="new_note_form"></textarea>';
		$this->comment_form_extra();
		echo '<button type="submit" class="button" form="new_note_form">' . __('Add note') . '</button>';
		add_action( 'admin_footer', [ $this, 'admin_footer' ] );
	}

	public function admin_footer() {
		echo '<form id="new_note_form" method="post" action="' . admin_url('admin-ajax.php?action=pcaddnote_' . $this->_post_type . '&id=' . get_the_ID()) . '"></form>';
	}

	public function add_note_ajax() {
		// TODO: Check permission
		wp_insert_comment([
			'comment_content' => $_POST['new_note'],
			'comment_post_ID' => $_GET['id'],
			'comment_type' => $this->_comment_type,
			'user_id' => get_current_user_id()
		]);
		$url = admin_url('post.php?action=edit&post=' . esc_attr($_GET['id']));
		wp_redirect($url);
		exit(0);
	}

}
