/**
 * PluginCore by Invisible Dragon
 * - Admin JS to activate built-in functionality
 */
(function($){
    $(document).ready(function() {

        function twoDigits(i) {
            if(i < 10) {
                return '0' + i;
            }
            return i;
        }

        // Activate any tabbed sections and autoselect first element
        $(".plugincore-tabs").each(function () {

            $("a", this).click(function (e) {
                let box = $(this).closest(".plugincore-tab-container");
                $("li", box).removeClass("active");
                $(this).closest("li").addClass("active");
                $(".plugincore-panel-tab", box).hide();
                $($(this).attr("href")).show();
                e.preventDefault();
            });

            $("li:first-child a", this).click();

        });

        // Field dependencies
        function updateFieldDependencies(){
            $(".plugincore-form-field").each(function() {
                let requires = $(this).data("requires");
                if (!requires) return;

                for(let require of requires) {
                    let val = $("[name=" + $(this).data("prefix") + "_" + require.key + "]").val();
                    let c = null;
                    switch (require.compare || '=') {
                        case '=':
                        default:
                            console.log(val, require.value);
                            c = val === require.value;
                    }
                    if(!c) {
                        $(this).hide();
                        return;
                    }
                }

                $(this).show();
            });
        }
        $("form input, form select").on("update change", updateFieldDependencies);
        updateFieldDependencies();

        // Media selector
        $(".plugincore-media").each(function () {
            let downloadable_file_frame = null;
            let self = this;

            $(".upload_file_button", this).click(function () {
                // If the media frame already exists, reopen it.
                if (downloadable_file_frame) {
                    downloadable_file_frame.open();
                    return;
                }

                // Create the media frame.
                let libraryType = '';
                if ($(self).data("type") === "image") {
                    libraryType = 'image';
                    console.log(libraryType);
                }
                downloadable_file_frame = wp.media.frames.downloadable_file = wp.media({
                    library: {
                        type: libraryType
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                downloadable_file_frame.on('select', function () {
                    let selection = downloadable_file_frame.state().get('selection');

                    selection.map(function (attachment) {
                        attachment = attachment.toJSON();
                        console.log(attachment);
                        $(".plugincore-raw-value", self).val(attachment.id);
                        $(".plugincore-selected-file", self).text(attachment.title);
                    });
                });

                if ($(self).data("private")) {
                    downloadable_file_frame.on('ready', function () {
                        downloadable_file_frame.uploader.options.uploader.params = {
                            type: 'plugincore_private'
                        };
                    });
                }

                // Finally, open the modal.
                downloadable_file_frame.open();
            });
        });

        // Single post selector
        $(".plugincore-select").each(function () {

            let self = this;
            let displayItem = function (item) {
                $(".plugincore-label", self).text(item.label);
                $(".input-text", self).hide();
                $(".plugincore-display").show();
            };

            let source = ajaxurl +
                "?action=plugincore_get_query_results&auth_path=" +
                encodeURIComponent($(this).data("auth-path")) +
                "&key=" +
                encodeURIComponent($(this).data("key"));
            $(".input-text", this).autocomplete({
                source: source,
                select: function (event, ui) {
                    displayItem(ui.item);
                    $(".plugincore-raw-value", self).val(ui.item.post_id);
                    $(this).val("");
                    event.preventDefault();
                }
            });
            $(".plugincore-display", this).hide();
            $(".button", this).click(function () {
                $(".plugincore-display", self).hide();
                $(".input-text", self).show();
            });

            if ($(this).data("item")) {
                displayItem($(this).data("item"));
            }

        });

        // Multi post selector
        let createPostItem = function (item) {
            let li = $("<li>").text(item.label).data("id", item.post_id);
            $("<span class=\"dashicons dashicons-menu\"></span>").prependTo(li);

            return li;
        };
        $(".plugincore-select-multiple").each(function () {

            let source = ajaxurl +
                "?action=plugincore_get_query_results&auth_path=" +
                encodeURIComponent($(this).data("auth-path")) +
                "&key=" +
                encodeURIComponent($(this).data("key"));

            let self = this;
            $.each($(this).data("items"), function (i, item) {
                createPostItem(item).appendTo($(".plugincore-item-list", self));
            });

            $(".plugincore-item-list", this).sortable().on("sortupdate", function () {
                let v = [];
                $("li", this).each(function () {
                    v.push($(this).data("id"));
                });
                $(".plugincore-raw-value", $(this).closest(".plugincore-select-multiple")).val(v.join(","));
            });

            $(".input-text", this).autocomplete({
                source: source,
                select: function (event, ui) {
                    let list = $(".plugincore-item-list", $(this).closest(".plugincore-select-multiple"));
                    createPostItem(ui.item).appendTo(list);
                    list.trigger("sortupdate");
                    $(this).val("");
                    event.preventDefault();
                }
            });
        });

        // DateTime (until all browsers have native support)
        $(".plugincore-form-field-datetime-local").each(function(){
            let c = $("input[type=datetime-local]", this);
            if(c.get(0).type === 'datetime-local') return; // Browser claiming support

            let v = new Date(c.hide().val());
            let day = $("<input>").attr({
                "type": "date",
                "pattern": "\d{4}-\d{2}-\d{2}",
                "value": v.getFullYear() + "-" + twoDigits(v.getMonth()+1) + "-" + twoDigits(v.getDate())
            }).insertAfter(c);
            let time = $("<input>").attr({
                "type": "time",
                "value": twoDigits(v.getHours()) + ":" + twoDigits(v.getMinutes())
            }).insertAfter(day);

            $("<span>").addClass("plugincore-timezone").text(Intl.DateTimeFormat().resolvedOptions().timeZone).insertAfter(time);

            let updateTime = function(){
                let d = new Date(day.val() + "T" + time.val());
                c.val(d.toISOString());
            };

            day.change(updateTime);
            time.change(updateTime);

        });

        // Basic repeater
        $(".plugincore-repeater").each(function(){
            $(".plugincore-add-item", this).click(function(){
                let box = $(this).closest(".plugincore-repeater");
                let tmpl = $(".plugincore-item-template", box).html();
                tmpl = tmpl.replaceAll('{TEMPLATE}', $(".plugincore-repeater-item", box).length);
                $("<div>").addClass("plugincore-repeater-item plugincore-card").html(tmpl).insertBefore(this);
            });
        });

    });

})(jQuery);
