// This allows for the WP Image upload controls to work properly outside of Gutenberg
// likely to be fixed in future

import { dispatch, select } from '@wordpress/data';
import { MediaUpload, MediaPlaceholder } from '@wordpress/media-utils';
import { addFilter } from '@wordpress/hooks';

require('@wordpress/core-data');

let settings = select('core/block-editor').getSettings();

dispatch('core/block-editor').updateSettings({
    ...settings,
    mediaUpload: true
});
addFilter(
    'editor.MediaUpload',
    'core/edit-site/components/media-upload',
    () => MediaUpload
);
addFilter(
    'editor.MediaPlaceHolder',
    'core/edit-site/components/media-upload',
    () => MediaPlaceholder
);
