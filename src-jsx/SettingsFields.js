import React, { Suspense } from 'react';
import controls from './controls';
import { applyFilters, addAction } from '@wordpress/hooks';

export default class SettingsFields extends React.Component {

    constructor(props) {
        super(props);

        let firstTab = null;
        if(props.tabs && props.tabs.length > 0) {
            firstTab = props.tabs[0].key;
        }
        this.state = { "data": props.data, "activeTab": firstTab };
    }

    componentDidMount() {
        addAction( 'plugincore_refresh', 'plugincore__pcPrefix', () => {
            this.forceUpdate();
        } );
    }

    render() {
        let data = this.state.data;
        let settingsFields = Object.keys( this.props.settings ).map( (fieldName) => {
            let field = this.props.settings[ fieldName ];
            let val = data[ fieldName ] || field['default'];
            if(val === null) {
                val = '';
            }
            field = Object.assign({
                'columns': '12',
                'label': field['title'],
                'value': val,
                'help': field['description'],
                'onChange': (value) => {
                    let newData = { ...this.state.data };
                    newData[ fieldName ] = value;
                    this.setState({
                        ...this.state,
                        'data': newData
                    });
                    this.props.onChange( newData );
                },
                'data-field-name': fieldName,
                'data-key': (this.props['parentPath'] || "") + fieldName,
                'data-auth-path': this.props['authPath']
            }, field);

            // Some sanitization/normalization
            if( field['options'] ) {
                if( !Array.isArray(field.options) ) {
                    // Convert to proper array
                    field.options = Object.keys( field.options ).map( (key) => {
                        return {
                            'label': field.options[key],
                            'value': key
                        }
                    });
                }
            }

            let className = "plugincore-field plugincore-field-" + field.type;

            let control = controls[field.type];
            control = applyFilters(
                // Prefix can override the control we pick
                'plugincore___pcPrefix_settings_field',
                // Global filter
                applyFilters( 'plugincore_settings_field', control, field.type, field ),
                field.type,
                field );
            if(!control) {
                control = controls['text'];
            }
            let fieldControl = React.createElement( control, field );

            let display = true;
            if(field['tab']) {
                if(this.state.activeTab !== field.tab) {
                    display = false;
                }
            }
            if(Array.isArray(field['requires'])) {
                field.requires.forEach((cond) => {
                    let val = applyFilters( 'plugincore_requires_value', data[cond.key], cond.key );
                    let comp = cond.compare || '=';
                    switch(comp) { // Note: opposite!
                        case '=':
                            if( val != cond.value ) {
                                display = false;
                            }
                            break;
                        case '!=':
                            if( val == cond.value ) {
                                display = false;
                            }
                            break;
                        case 'IN':
                            if(cond.value.indexOf(val) === -1){
                                display = false;
                            }
                            break;
                    }
                });
            }

            return <div key={fieldName} className={className} style={ { "gridColumn": "span " + field['columns'], "display": display ? 'block' : 'none' } }>
                <Suspense fallback={<progress></progress>}>{fieldControl}</Suspense>
            </div>;
        } );

        let settings = (
            <div className="plugincore-settings-fields"
                style={ { display: "grid", "gridTemplateColumns": "repeat(12, 1fr)", "gridGap": "1rem" } }>
                {settingsFields}
            </div>
        );
        if(this.props.tabs && this.props.tabs.length > 0) {
            let tabs = this.props.tabs.map((tab) => {
                let setTab = () => {
                    this.setState({
                        ...this.state,
                        activeTab: tab.key
                    });
                };
                return <li key={tab.key} className={this.state.activeTab === tab.key ? 'active' : ''}>
                    <a onClick={setTab} href="#">{tab.icon && <i className={tab.icon + " dashicons"}></i> } {tab.label}</a>
                </li>
            });
            return <div className="plugincore-tab-container">
                <ul className="plugincore-tabs">
                    {tabs}
                </ul>
                <div className="plugincore-tab-content">{settings}</div>
            </div>
        } else {
            return settings;
        }
    }

}
