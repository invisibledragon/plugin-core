import {blockStyle} from "./index";

import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import SettingsFields from "./SettingsFields";

const Block = (blockData) => {
    return {
        ...blockData,
        attributes: {
            data: {
                type: 'object',
                default: {}
            },
            align: {
                type: 'string',
                default: blockData['defaultAlign'] || 'wide'
            }
        },
        supports: {
            ...(blockData['supports'] || {}),
            html: false
        },
        "editorScript": "file:./index.js",
        edit( { attributes, setAttributes, isSelected } ) {
            const blockProps = useBlockProps( { } );

            let updateData = (newData) => {
                setAttributes({
                    "data": newData,
                    test: "banana"
                });
            }

            let aboveSettings = null,
                belowSettings = null,
                settingsStyle = {
                    "padding": "10px"
                };

            switch(blockData.settings.inner_blocks) {
                case 'top':
                    aboveSettings = <InnerBlocks {...blockData.settings.inner_block_settings} />;
                    break;
                case 'right':
                    settingsStyle['display'] = 'grid';
                    settingsStyle['gridTemplateColumns'] = 'repeat(2, 1fr)';
                    settingsStyle['gap'] = '1rem';
                case 'bottom':
                    belowSettings = <InnerBlocks {...blockData.settings.inner_block_settings} />;
                    break;
            }

            return (
                <div { ...blockProps } style={{
                    "margin": "0.4rem 0"
                }}>
                    <div className="plugincore-block" style={{
                        "border": "1px solid var(--wp-admin-theme-color)",
                        "borderRadius": "5px"
                    }}>
                        <span style={{
                            "backgroundColor": "var(--wp-admin-theme-color)",
                            "color": "#fff",
                            "padding": "5px 10px",
                            "display": "inline-block",
                            "fontSize": "12px",
                            "fontFamily": "sans-serif",
                            "borderBottomRightRadius": "5px"
                        }} className="plugincore-block-name">{ blockData.title }</span>
                        <div style={settingsStyle}>
                            {aboveSettings}
                            <SettingsFields
                                settings={ blockData.fields }
                                onChange={ updateData }
                                data={attributes.data} />
                            {belowSettings}
                        </div>
                    </div>
                </div>
            );
        },
        save() {
            const blockProps = useBlockProps.save();

            return (
                <InnerBlocks.Content />
            );
        }
    }
}

export default Block;
