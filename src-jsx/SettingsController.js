import SettingsFields from "./SettingsFields";
import ReactDOM from 'react-dom';

import {  select } from '@wordpress/data';

// This allows for the WP Image upload controls to work properly outside of Gutenberg
// likely to be fixed in future
let settings = select('core/block-editor').getSettings();
if(settings.mediaUpload === undefined) {
    require('./fix_non_gutenberg_images.js');
}

const SettingsController = (controller) => {

    let d = document.createElement("div");
    d.classList.add("plugincore-settings-controller");
    controller.append(d);

    let settings = JSON.parse(controller.getAttribute("data-fields"));
    let tabs = JSON.parse(controller.getAttribute("data-tabs"));
    let dataField = controller.getElementsByTagName("input")[0];
    let data = JSON.parse(dataField.getAttribute("data-orig"));
    let authPath = controller.getAttribute("data-auth-path");

    let onChange = (newData) => {
        data = newData;
        dataField.value = JSON.stringify(newData);
    };

    // let r = ReactDOM.createRoot( d );
    // r.render( <SettingsFields settings={settings} data={data} onChange={onChange} /> )
    ReactDOM.render( <SettingsFields settings={settings} authPath={authPath} tabs={tabs} data={data} onChange={onChange} />, d );

}

export default SettingsController;
