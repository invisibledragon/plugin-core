import { TextControl, RadioControl, SelectControl, TextareaControl, __experimentalAlignmentMatrixControl as AlignmentMatrixControl } from '@wordpress/components';
import React from "react";

import {WrappedCheckboxControl} from "./wrapped/WrappedCheckboxControl";
import {WrappedDateTimeControl} from "./wrapped/WrappedDateTimeControl";
import {WrappedColorControl} from "./wrapped/WrappedColorControl";

import { RepeaterControl } from "./controls/RepeaterControl";
import {PostControl} from "./controls/PostControl";
import {MultiPostControl} from "./controls/MultiPostControl";
import {ImageControl} from "./controls/ImageControl";
import {ImageGalleryControl} from "./controls/ImageGalleryControl";
import {NoticeControl} from "./controls/NoticeControl";
import {ModalGroupControl} from "./controls/ModalGroupControl";
import {MultiselectControl} from "./controls/MultiselectControl";
import {TextArrayControl} from "./controls/TextArrayControl";
const RichtextControl = React.lazy(() => import('./controls/RichtextControl') );

export default {
    "text": TextControl,
    "textarray": TextArrayControl,
    "alignment": AlignmentMatrixControl,
    "radio": RadioControl,
    "checkbox": WrappedCheckboxControl,
    "select": SelectControl,
    "textarea": TextareaControl,
    "repeater": RepeaterControl,
    "datetime": WrappedDateTimeControl,
    "post": PostControl,
    "posts": MultiPostControl,
    "color": WrappedColorControl,
    "image": ImageControl,
    "gallery": ImageGalleryControl,
    "notice": NoticeControl,
    "modal": ModalGroupControl,
    "richtext": RichtextControl,
    "multiselect": MultiselectControl
}
