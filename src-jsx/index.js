/**
 * WordPress dependencies
 */
import { registerBlockType } from '@wordpress/blocks';

import Block from './block';
import SettingsController from "./SettingsController";

import { doAction, addFilter } from '@wordpress/hooks';

let infoKey = 'plugincore___pcPrefix';

if(window[infoKey]) {
    let scriptData = window[infoKey];

    if(scriptData['blocks']) {
        for (const block of scriptData.blocks) {
            const {name} = block;
            // Register the block
            registerBlockType(name, Block(block));
        }
    }
} else {
    console.log("An instance of PluginCore was loaded however no data was found. Prefix of __pcPrefix");
}

// Load any Settings Fields
window.addEventListener('DOMContentLoaded', () => {

    if(document.getElementById("poststuff")) {
        // This looks like an edit post type
        let parentSelect = document.getElementById("parent_id");
        let parentPage = parentSelect ? parentSelect.value : 0;
        if(parentSelect) {
            parentSelect.addEventListener("change", function() {
                parentPage = parentSelect.value;
                doAction( 'plugincore_refresh' );
            });
        }

        let templateSelect = document.getElementById("page_template");
        let pageTemplate = templateSelect ? templateSelect.value : 'default';
        if(templateSelect) {
            templateSelect.addEventListener("change", function() {
                pageTemplate = templateSelect.value;
                doAction( 'plugincore_refresh' );
            });
        }

        addFilter( 'plugincore_requires_value', 'plugincore__pcPrefix', function(val, key) {
            switch(key) {
                case 'parent_id':
                    return parentPage;
                case 'page_template':
                    return pageTemplate;
            }
            return val;
        } );
    }

    let settingsBlocks = document.getElementsByClassName("plugincore__pcPrefix-settings-fields");
    for (let block of settingsBlocks) {
        SettingsController(block);
    }
});
