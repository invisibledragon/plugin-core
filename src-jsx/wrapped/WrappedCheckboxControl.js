import React from "react";
import { CheckboxControl } from '@wordpress/components';

/**
 * This class wraps CheckboxControl so that we can use value instead of checked
 */
export class WrappedCheckboxControl extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <CheckboxControl
            checked={ this.props.value }
            {...this.props} />
    }

}
