import React from "react";
import { DateTimePicker, BaseControl  } from '@wordpress/components';

/**
 * This class wraps DateTime so that we can use value
 */
export class WrappedDateTimeControl extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <BaseControl {...this.props}>
            <DateTimePicker
                currentDate={ this.props.value }
                {...this.props} />
        </BaseControl>
    }

}
