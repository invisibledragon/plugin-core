import React from "react";
import { ColorPicker, BaseControl  } from '@wordpress/components';

/**
 * This class wraps ColorControl so that we can use value
 */
export class WrappedColorControl extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <BaseControl {...this.props}>
            <ColorPicker
                color={ this.props.value }
                {...this.props} />
        </BaseControl>
    }

}
