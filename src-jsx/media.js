/**
 * This is a service to fetch media from AJAX call but batches up requests instead of overloading
 * backend service
 */

const API_CALL = '/wp-admin/admin-ajax.php?action=plugincore_get_media';

let timer = null,
    waitingList = {};

function fetchMediaList() {
    let batchList = waitingList;
    waitingList = {}; // Reset in the meantime!
    window.fetch(API_CALL + "&media=" + Object.keys(batchList).join(";"))
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            for(let item of data) {
                if(batchList[item.id]) {
                    for(let cb of batchList[item.id]) {
                        cb(item);
                    }
                }
            }
            batchList = [];
        }).catch(function(error) {
            // TODO: handle error??
            console.log(error);
        });
 }

export default function getMedia(media_id, cb) {

    if(!waitingList[media_id]){
        waitingList[media_id] = [];
    }
    waitingList[media_id].push(cb);
    if(timer !== null) {
        clearTimeout(timer);
    }
    timer = setTimeout(fetchMediaList, 100);

}
