import React from "react";

import { BaseControl, Button, Icon } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

import {PostControl} from "./PostControl";

export class MultiPostControl extends React.Component {

    constructor(props) {
        super(props);
        let val = props.value;
        if(val === '' || val === null || val === undefined) {
            val = [];
        }
        this.state = { value: val };
    }

    render() {
        let items = this.state.value.map( (val, key) => {

            let changeVal = (newVal) => {
                let items = this.state.value;
                items[key] = newVal;
                this.setState({...this.state, value: items});
                this.props.onChange(items);
            };

            let removeVal = () => {
                let items = this.state.value;
                items.splice( key, 1 );
                this.setState({...this.state, value: items});
                this.props.onChange(items);
            };

            return <div key={key} className="plugincore-multi-post-post">
                <PostControl
                    onChange={changeVal}
                    data-key={this.props['data-key']}
                    data-auth-path={this.props['data-auth-path']}
                    options={this.props.options || []}
                    value={val} />
                <Button variant="secondary" title={__('Delete item')} onClick={removeVal}>
                    <Icon icon="trash" />
                </Button>
            </div>;

        } );

        let newItem = () => {
            let items = this.state.value;
            items.push(null);
            this.setState({ ...this.state, value: items });
        };

        return <BaseControl {...this.props}>
            <div className="plugincore-multi-post">
                {items}
            </div>
            <Button variant="secondary" onClick={newItem}>{ __('New Item') }</Button>
        </BaseControl>
    }

}
