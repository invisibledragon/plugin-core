import React from "react";
import { BaseControl, Button, Modal } from '@wordpress/components';
import SettingsFields from "../SettingsFields";
import { __ } from '@wordpress/i18n';

export class ModalGroupControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = { open: false };
    }

    render() {

        let open = () => {
            this.setState({
                ...this.state,
                open: true
            });
        }

        let close = () => {
            this.setState({
                ...this.state,
                open: false
            });
        }

        let updateData = (newData) => {
            this.props.onChange(newData);
        }

        return <BaseControl {...this.props}>
            { this.state.open && <Modal
                style={ { width: '90vw', height: '90vh' } }
                title={this.props.title}
                onRequestClose={close}>
                <SettingsFields
                    settings={ this.props.fields }
                    onChange={ updateData }
                    data={ this.props.value || {} } />
            </Modal> }
            <div>
                <Button variant="secondary" onClick={open}>{ __('Open') }</Button>
            </div>
        </BaseControl>
    }

}
