import React from "react";
import { __ } from '@wordpress/i18n';
import { MediaUploadCheck, MediaUpload } from '@wordpress/block-editor';
import { BaseControl, Button, ResponsiveWrapper } from '@wordpress/components';
import { select, resolveSelect } from '@wordpress/data';
import getMedia from '../media';

export class ImageControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value || 0,
            media: this.props.media
        }
        this.findMediaDetails = this.findMediaDetails.bind(this);
    }

    componentDidMount() {
        if(this.state.value && !this.state.media) {
            this.findMediaDetails();
        }
    }

    findMediaDetails() {
        getMedia(this.state.value, (v) => {
            this.setState({
                ...this.state,
                media: v
            });
        })
    }

    render() {

        const onSelectMedia = (media) => {
            this.setState({
                value: media.id,
                media_url: media.url
            });
            this.props.onChange( media.id );
            this.findMediaDetails();
        }

        const removeMedia = () => {
            this.setState({
                value: 0,
                media_url: null
            });
            this.props.onChange( null );
        }

        return <BaseControl {...this.props}>
            <MediaUploadCheck>
                <MediaUpload
                    allowedTypes={ ['image'] }
                    onSelect={onSelectMedia}
                    value={this.state.value}
                    render={({open}) => (
                        <Button
                            className={this.state.value == 0 ? 'editor-post-featured-image__toggle' : 'plugincore-image-thumb-button'}
                            onClick={open}
                        >
                            {this.state.value == 0 && __('Choose an image')}
                            {this.state.media != undefined &&
                                <div class={"plugincore-image-thumb-holder"}>
                                    <img src={this.state.media.source_url}/>
                                </div>
                            }
                        </Button>
                    )}
                />
            </MediaUploadCheck>
            {this.state.value != 0 &&
                <MediaUploadCheck>
                    <MediaUpload
                        title={__('Replace image', 'awp')}
                        value={this.state.value}
                        onSelect={onSelectMedia}
                        allowedTypes={['image']}
                        render={({open}) => (
                            <Button onClick={open} isDefault>{__('Replace image', 'awp')}</Button>
                        )}
                    />
                </MediaUploadCheck>
            }
            {this.state.value != 0 &&
                <MediaUploadCheck>
                    <Button onClick={removeMedia} isLink isDestructive>{__('Remove image', 'awp')}</Button>
                </MediaUploadCheck>
            }
        </BaseControl>
    }

}
