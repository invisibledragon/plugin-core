import React from "react";
import { BaseControl, ComboboxControl, Button, Icon } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

/**
 * Select Multiple items
 */
export class MultiselectControl extends React.Component {

    constructor(props) {
        super(props);
        let val = props.value;
        if(!Array.isArray(val)) {
            if(val !== null && val !== ""){
                val = [val];
            } else {
                val = [];
            }
        }
        this.state = {
            value: val
        }
    }

    render() {
        let items = this.state.value.map( (val, key) => {

            let removeVal = () => {
                let items = this.state.value;
                items.splice(key, 1);
                this.setState({...this.state, value: items});
                this.props.onChange(items);
            };

            let display = this.props.options.filter((p) => {
                return p.value == val;
            });
            display = display.length ? display[0].label : val;

            return <div key={key} className="plugincore-item" style={{"display": "flex"}}>
                <div style={{"flexGrow": "1"}}>{display}</div>
                <Button variant="secondary" title={__('Delete item')} onClick={removeVal}>
                    <Icon icon="trash"/>
                </Button>
            </div>;
        });

        let setValue = (v) => {
            if(!v) return; // Do nothing

            let items = this.state.value;
            items.push(v);
            this.setState({...this.state, value: items});
            this.props.onChange(items);
        };

        return <BaseControl {...this.props}>
            <div className={"plugincore-multi-list"}>
                {items}
            </div>
            <ComboboxControl
                label={__('Add new item')}
                allowReset={true}
                onChange={setValue}
                options={this.props.options} />
        </BaseControl>;
    }

}
