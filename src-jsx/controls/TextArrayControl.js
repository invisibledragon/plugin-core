import React from "react";
import { BaseControl, ComboboxControl, Button, Icon } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

/**
 * Select Multiple items
 */
export class TextArrayControl extends React.Component {

	constructor(props) {
		super(props);
		let val = props.value;
		if(!Array.isArray(val)) {
			if(typeof val === "object") {
				val = Object.values(val);
			} else if(val !== null && val !== ""){
				val = [val];
			} else {
				val = [];
			}
		}
		this.state = {
			value: val
		}
	}

	render() {

		let items = this.state.value.map( (val, key) => {

			let removeVal = () => {
				let items = this.state.value;
				items.splice(key, 1);
				this.setState({...this.state, value: items});
				this.props.onChange(items);
			};

			let updateItem = (e) => {
				let items = this.state.value;
				items[key] = e.target.value;
				this.setState({...this.state, value: items});
				this.props.onChange(items);
			};

			return <div key={key} className="plugincore-item" style={{"display": "flex"}}>
				<div style={{"flexGrow": "1"}}>
					<input style={{"display":"block"}} type="text" value={val} onChange={updateItem} />
				</div>
				<Button variant="secondary" title={__('Delete item')} onClick={removeVal}>
					<Icon icon="trash"/>
				</Button>
			</div>;
		});

		let newValue = () => {
			let items = this.state.value;
			items.push('');
			this.setState({...this.state, value: items});
			this.props.onChange(items);
		};

		return <BaseControl {...this.props}>
			<div className={"plugincore-multi-list"}>
				{items}
			</div>
			<Button onClick={newValue}>{__('Add new item')}</Button>
		</BaseControl>;
	}

}
