import React from "react";
import { BaseControl  } from '@wordpress/components';

import { Editor } from '@tinymce/tinymce-react';

import { debounce } from "lodash";

let scriptKey = 'plugincore___pcPrefix';

export default class RichtextControl extends React.Component {
    static editorCounter = 0;

    constructor(props) {
        super(props);
        this.state = {
            value: props.value || "",
            id: "pc-tmce-___pcPrefix-" + RichtextControl.editorCounter
        };
        RichtextControl.editorCounter += 1;
        this.editorRef = React.createRef();
    }

    render() {
        let notifyUpdate = debounce(() => {
            this.props.onChange(this.editorRef.current.getContent());
        }, 200);

        let toolbar = this.state.toolbar || "Full";
        toolbar = window[scriptKey]['tinymceToolbars'][ toolbar ];
        let args = tinyMCEPreInit.mceInit.plugincore_tinymce;

        if(toolbar) {
            for (let i = 1; i <= 4; i++) {
                args['toolbar' + i] = (toolbar[i] || []).join(" | ");
            }
        }

        let finishSetup = () => {
            // TinyMCE react seems to set this up for us, but we want to add a class to it!
            let textarea = document.getElementById(this.state.id);
            textarea.classList.add("wp-editor-area");
            textarea.addEventListener("change", () => {
                // autop so it's similar to tinymce
                this.props.onChange(wp.editor.autop(textarea.value));
            });
        }

        args['id'] = this.state.id;

        return <BaseControl {...this.props}>
            <div id={"wp-" + this.state.id + "-wrap"} className={"wp-core-ui wp-editor-wrap tmce-active"}>
                <div id={"wp-editor-tools-" + this.state.id} className="wp-editor-tools">
                    <div className="wp-media-buttons">
                        <button type="button" data-editor={this.state.id} className="button insert-media add_media">
                            <span className="wp-media-buttons-icon"></span>
                            Add Media
                        </button>
                    </div>
                    <div className="wp-editor-tabs">
                        <button className="wp-switch-editor switch-tmce" data-wp-editor-id={this.state.id} type="button">Visual</button>
                        <button className="wp-switch-editor switch-html" data-wp-editor-id={this.state.id} type="button">Text</button>
                    </div>
                </div>
                <div id={"wp-" + this.state.id + "-editor-container"} className="wp-editor-container">
                    <Editor
                        tinymceScriptSrc={ window[scriptKey]['tinymceScriptSrc'] }
                        id={this.state.id}
                        onInit={(evt, editor) => this.editorRef.current = editor}
                        init={{
                            ...args,
                            wpautop: true,
                            height: 500,
                            menubar: false,
                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
                            setup(ed) {
                                ed.on('change', function (e) {
                                    ed.save(); // save to textarea
                                    notifyUpdate();
                                });

                                // Fix bug where Gutenberg does not hear "mouseup" event and tries to select multiple blocks.
                                ed.on('mouseup', function (e) {
                                    var event = new MouseEvent('mouseup');
                                    window.dispatchEvent(event);
                                });

                                finishSetup();

                                ed.on('init', function () {
                                    ed.execCommand('wpAutoResizeOn');
                                });

                            }
                        }}
                        initialValue={this.state.value} />
                </div>
            </div>
        </BaseControl>
    }

}
