import React from "react";

import { BaseControl, Button, Icon, ResponsiveWrapper, TextControl } from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import { MediaUploadCheck, MediaUpload } from '@wordpress/block-editor';
import { select, resolveSelect } from '@wordpress/data';
import { addQueryArgs } from '@wordpress/url';

import {PostControl} from "./PostControl";
import getMedia from "../media";

// FontAwesome Icon for default usage
const BLANK_MEDIA = 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 576 512\'%3E%3Cpath d=\'M528 64h-480C21.49 64 0 85.49 0 112v288C0 426.5 21.49 448 48 448h480c26.51 0 48-21.49 48-48v-288C576 85.49 554.5 64 528 64zM68 432L185.6 275.2c3.031-4.094 9.75-4.094 12.78 0l39.6 52.8L159.1 432H68zM180 432l165.6-220.8c3.031-4.094 9.75-4.094 12.78 0L524 432H180zM560 400c0 12.75-7.627 23.59-18.45 28.73L371.2 201.6c-9.125-12.22-29.28-12.22-38.41 0L247.1 314.7L211.2 265.6c-9.125-12.22-29.28-12.22-38.41 0L47.1 432C30.36 432 16 417.6 16 400v-288c0-17.64 14.36-32 32-32h480c17.64 0 32 14.36 32 32V400zM128 144C101.5 144 80 165.5 80 192S101.5 240 128 240S176 218.5 176 192S154.5 144 128 144zM128 224C110.4 224 96 209.6 96 192s14.36-32 32-32s32 14.36 32 32S145.6 224 128 224z\'/%3E%3C/svg%3E';

class ImageGalleryItemControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value || 0,
            media: this.props.media,
            media_caption: '',
            media_updating: false
        }
        this.findMediaDetails = this.findMediaDetails.bind(this);
    }

    findMediaDetails() {
        getMedia(this.state.value, (v) => {
            this.setState({
                ...this.state,
                media: v,
                media_caption: v.caption
            });
        });
    }

    componentDidMount() {
        if(this.state.value && !this.state.media) {
            this.findMediaDetails();
        }
    }

    render() {

        let setActive = () => {
            this.props.setActive();
        }

        let updateMediaCaption = (n) => {
            this.setState({...this.state, media_caption: n});
        }

        let sendMediaUpdateCaption = () => {
            this.setState({...this.state, media_updating: true});
            let b = new FormData();
            b.append('caption', this.state.media_caption);
            fetch(addQueryArgs(window.ajaxurl || '/wp-admin/admin-ajax.php', {
                action: 'plugincore_update_media',
                media_id: this.state.media.id
            }), {
                method: "POST",
                body: b
            }).then((response) => response.json()).then((data) => {
                this.setState({...this.state, media_updating: false});
            });
        }

        return <div onClick={setActive} className="plugincore-gallery-item">
            {this.state.media != undefined &&
                <img src={this.state.media.source_url}/>
            }
            {this.state.media == undefined &&
                <img className="loading" src={BLANK_MEDIA} />
            }
            {
                this.props.isActive && !this.state.media_updating &&
                <div>
                    <TextControl label={__('Caption')} value={this.state.media_caption} onChange={updateMediaCaption} />
                    <Button variant="secondary" onClick={sendMediaUpdateCaption}>
                        {__('Update')}
                    </Button>
                </div>
            }
            {
                this.props.isActive && this.state.media_updating &&
                <div>
                    {__('Updating...')}
                </div>
            }
        </div>
    }
}

export class ImageGalleryControl extends React.Component {

    constructor(props) {
        super(props);
        let val = props.value;
        if(val === '' || val === undefined || val === null) {
            val = [];
        }
        this.state = { value: val, activeItem: null };
    }

    render() {
        let items = this.state.value.map( (val, key) => {

            let changeVal = (newVal) => {
                let items = this.state.value;
                items[key] = newVal;
                this.setState({...this.state, value: items});
                this.props.onChange(items);
            };

            let removeVal = () => {
                let items = this.state.value;
                items.splice( key, 1 );
                this.setState({...this.state, value: items});
                this.props.onChange(items);
            };

            let setActive = () => {
                this.setState({ ...this.state, activeItem: key });
            }

            return <div className={this.state.activeItem === key ? 'active' : ''} key={key}>
                <ImageGalleryItemControl
                    onChange={changeVal}
                    options={this.props.options || []}
                    setActive={setActive}
                    isActive={this.state.activeItem === key}
                    value={val} />
                <Button variant="secondary" className="delete-button" title={__('Delete item')} onClick={removeVal}>
                    <Icon icon="trash" />
                </Button>
            </div>;

        } );

        let onSelectMedia = (media) => {
            let items = this.state.value;
            for(let item of media) {
                items.push(item.id);
            }
            this.setState({ ...this.state, value: items });
            this.props.onChange(items);
        };

        return <BaseControl {...this.props}>
            <div className="plugincore-gallery">
                {items}
            </div>
            <MediaUploadCheck>
                <MediaUpload
                    allowedTypes={ ['image'] }
                    multiple={true}
                    onSelect={onSelectMedia}
                    render={({open}) => (
                        <Button
                            variant="secondary"
                            onClick={open}>
                            {__('Choose an image')}
                        </Button>
                    )}
                />
            </MediaUploadCheck>
        </BaseControl>
    }

}
