import React, { useState } from "react";

import { __ } from '@wordpress/i18n';
import { Button, BaseControl, Icon } from '@wordpress/components';

import SettingsFields from "../SettingsFields";

function randstr(prefix) {
    return Math.random().toString(36).replace('0.',prefix || '');
}

function RepeaterItem(props) {
    const item = props.item;

    let updateItem = (newData) => {
        props.onChange(newData, props.index);
    };
    let removeThis = () => {
        if(confirm(__('Are you sure you wish to delete this item?'))) {
            props.onDelete( props.index );
        }
    };
    let moveUp = () => {
        props.onMoveUp(props.index);
    };
    let moveDown = () => {
        props.onMoveDown(props.index);
    };
    let newItem = () => {
        props.onNewItem(props.index);
    };

    return <div key={item['_id']}>
        <div className="plugincore-repeater-card">
            <SettingsFields settings={props.fields} data={item} onChange={updateItem} authPath={props['data-auth-path']} parentPath={props.parent} />
            <div className="plugincore-card-controls">
                <Button variant="secondary" title={__('Delete item')} onClick={removeThis}>
                    <Icon icon="trash" />
                </Button>
                { props.index > 0 && <Button variant="secondary" title={__('Move item up')} onClick={moveUp}>
                    <Icon icon="arrow-up" />
                </Button> }
                { props.index < props.listLength-1 && <Button variant="secondary" title={__('Move item down')} onClick={moveDown}>
                    <Icon icon="arrow-down" />
                </Button> }
            </div>
        </div>
        { props.index < props.listLength-1 && <Button className="plugincore-add-between-button" variant="secondary" title={__('Add new item here')} onClick={newItem}>
            <Icon icon="plus" />
        </Button> }
    </div>;

}

export function RepeaterControl( props ) {
    let val = props.value || [];
    val = val.map((item) => {
        if(!item['_id']) {
            item['_id'] = randstr('d');
        }
        return item;
    });

    const [ data, setData ] = useState( val );

    const changeItem = (newData, index) => {
        let newItem = data;
        newItem[index] = newData;
        setData( newItem );
        props.onChange(newItem);
    }
    const deleteItem = (index) => {
        let newData = data;
        newData.splice(index, 1);
        setData(newData);
        props.onChange(newData);
    };

    const moveItemUp = (index) => {
        data.splice(index-1, 0, ...data.splice(index, 1));
        setData(data);
        props.onChange(data);
    };
    const moveItemDown = (index) => {
        data.splice(index+1, 0, ...data.splice(index, 1));
        setData(data);
        props.onChange(data);
    };

    const newSlicedItem = (index) => {
        let newData = data;
        newData.splice(index+1, 0,{ _id: randstr('e') });
        setData(newData);
        props.onChange(newData);
    }

    let items = data.map((item, index) => {
        let fields = props.fields;
        if(props.per_result && props.per_result[index]) {
            fields = Object.assign(fields, props.per_result[index]);
        }
        return <RepeaterItem
            item={item}
            listLength={data.length}
            index={index}
            onDelete={deleteItem}
            onChange={changeItem}
            onMoveUp={moveItemUp}
            onMoveDown={moveItemDown}
            onNewItem={newSlicedItem}
            fields={fields}
            parent={props['data-field-name'] + "."}
            data-auth-path={props['data-auth-path']}
        />
    });

    let newItem = () => {
        let newData = data;
        newData.push({ _id: randstr('e') });
        setData(newData);
        props.onChange(newData);
    }

    return <BaseControl {...props} className="plugincore-repeater">
        <div>
            <div style={ { position: "relative" } }>
                {items}
            </div>
            <Button variant="secondary" onClick={newItem}>{ __('New Item') }</Button>
        </div>
    </BaseControl>;
}

