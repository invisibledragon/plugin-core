import { Notice } from '@wordpress/components';

export function NoticeControl( props ) {

    return <Notice status={props.status || "info"} isDismissible={false}>
        { props.message }
    </Notice>

}
