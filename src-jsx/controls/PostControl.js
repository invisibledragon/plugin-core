import React from "react";
import { BaseControl, ComboboxControl } from '@wordpress/components';
import { addQueryArgs } from '@wordpress/url';

import debounce from 'lodash.debounce';

export class PostControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = { options: props.options || [], loading: false };
        this.filterChange = debounce(this.filterChange.bind(this), 100);
    }

    filterChange = (search) => {
        if(search === '') {
            this.setState({
                ...this.state,
                options: []
            });
        } else {
            if(!this.state.loading) {
                this.setState({
                    ...this.state,
                    loading: true
                });
                fetch(addQueryArgs(window.ajaxurl || '/wp-admin/admin-ajax.php', {
                    action: 'plugincore_get_query_results',
                    auth_path: this.props['data-auth-path'],
                    key: this.props['data-key'],
                    term: search
                })).then((response) => response.json()).then((data) => {
                    if(data.status == "_fail") return; // Something went wrong
                    this.setState({
                        ...this.state,
                        options: data,
                        loading: false
                    });
                });
            }
        }
    }

    render() {

        let onChange = (val) => {
            this.props.onChange( val );
        }

        return (
            <BaseControl {...this.props}>
                <ComboboxControl
                    value={this.props.value}
                    onChange={onChange}
                    options={this.state.options || []}
                    onFilterValueChange={this.filterChange} />
            </BaseControl>
        );
    }


}
