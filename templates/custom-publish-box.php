<label for="poststatus">
	<?= __('Status'); ?>
</label>
<select id="poststatus" name="post_status" style="display: block;margin:0.5rem 0">
	<?php
	$status = static::get_custom_post_statuses();
	foreach($status as $key => $label) {
		echo '<option value="' . esc_attr($key) . '"';
		selected($post->post_status, $key);
		echo '>' . esc_html($label) . '</option>';
	}
	?>
</select>

<div id="major-publishing-actions">
	<?php
	$post_type        = $post->post_type;
	$post_type_object = get_post_type_object( $post_type );
	$can_publish      = current_user_can( $post_type_object->cap->publish_posts );

	/**
	 * Fires at the beginning of the publishing actions section of the Publish meta box.
	 *
	 * @since 2.7.0
	 * @since 4.9.0 Added the `$post` parameter.
	 *
	 * @param WP_Post|null $post WP_Post object for the current post on Edit Post screen,
	 *                           null on Edit Link screen.
	 */
	do_action( 'post_submitbox_start', $post );
	?>
	<div id="delete-action">
		<?php
		if ( current_user_can( 'delete_post', $post_id ) ) {
			if ( ! EMPTY_TRASH_DAYS ) {
				$delete_text = __( 'Delete permanently' );
			} else {
				$delete_text = __( 'Move to Trash' );
			}
			?>
			<a class="submitdelete deletion" href="<?php echo get_delete_post_link( $post_id ); ?>"><?php echo $delete_text; ?></a>
			<?php
		}
		?>
	</div>

	<div id="publishing-action">
		<span class="spinner"></span>
		<input name="original_publish" type="hidden" id="original_publish" value="<?php esc_attr_e( 'Update' ); ?>" />
		<?php submit_button( __( 'Update' ), 'primary large', 'save', false, array( 'id' => 'publish' ) ); ?>
	</div>
	<div class="clear"></div>
</div>
