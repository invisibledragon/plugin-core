<div class="wrap">

    <h1>
        <?= static::get_label(); ?>
    </h1>

    <hr/>

    <table class="widefat">
        <thead>
        <tr>
        <?php
        $headers = static::get_columns();
        foreach($headers as $key => $label) {
            echo '<td data-column-id="' . esc_attr($key) . '">' . esc_html($label) . '</td>';
        }
        ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $items = static::get_items(null, null, [ 20, @$_GET['offset'] ?: 0 ]);
        if(empty($items)) {
            echo '<tr><td colspan="' . count($headers) . '">' . __('There are no items to display') . '</td></tr>';
        }
        foreach($items as $item) {
            echo '<tr>';
            foreach($headers as $key => $label) {
                echo '<td data-column-id="' . esc_attr($key) . '">';
                static::get_custom_column( $key, $item );
                echo '</td>';
            }
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>

    <a href="<?= add_query_arg('offset', (@$_GET['offset'] ?: 0) + 20); ?>">
        <?= __('Next Page'); ?>
    </a>

</div>
