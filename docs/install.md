# Installation

**Important:**

While PluginCore is delivered by Composer, please follow these steps to ensure
a conflict-free and smooth experience!

Composer.json:

```json
{
  "dependencies": {
    "typisttech/imposter-plugin": "^0.6.2",
    "invisibledragon/plugincore": "dev-master"
  },
  "extra": {
    "imposter": {
      "namespace": "Your\\NamespaceNameHere",
      "excludes": [
        ""
      ]
    }
  },
  "scripts": {
    "post-install-cmd": [
      "php ./vendor/bin/setup-pc.php YOURPREFIX"
    ]
  }
}
```
