#!/usr/bin/env php
<?php

echo "PluginCore - Setup Utility\n";
if(count($_SERVER['argv']) == 1) {
	return die("Please pass in the prefix you wish to use with PluginCore");
}
$prefix = $_SERVER['argv'][1];

echo "Prefix: " . $prefix . "\n";



@mkdir(dirname(__FILE__) . "/../assets/jsx/");

// Check if we have our downloaded files
echo "> Downloading JSX from Gitlab...\n";
$baseurl = 'https://invisibledragon.gitlab.io/plugin-core/';
$file = 'output.zip';
$contents = file_get_contents( $baseurl . $file );
file_put_contents( dirname(__FILE__) . "/../assets/jsx/" . $file, $contents );
$zip = new ZipArchive;
$res = $zip->open(dirname(__FILE__) . "/../assets/jsx/" . $file);
if ($res === TRUE) {
	$zip->extractTo(dirname(__FILE__) . "/../assets/jsx/");
	$zip->close();
	echo '> OK';
} else {
    echo "> FAIL";
	return die(1);
}

// Update prefix in files
echo "\n> Update prefix in files\n";
$files = glob(dirname(__FILE__) . "/../assets/jsx/*.js");
foreach($files as $file) {
	$contents = file_get_contents( $file );
	$contents = str_replace( '__pcPrefix', $prefix, $contents );
	file_put_contents( $file, $contents );
}

// Update prefix in index.asset.php
echo "> Update prefix in index.asset.php\n";
$asset_php = include( dirname(__FILE__) . "/../assets/jsx/index.asset.php" );
$asset_php['prefix'] = $prefix;
file_put_contents( dirname(__FILE__) . "/../assets/jsx/index.asset.php", '<?php return ' . var_export($asset_php, true) . ';' );
echo "Done\n";
